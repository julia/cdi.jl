using cdi.cdiDatasets
using Dates

# help function to test a variable name
hasVar(ds, name) = haskey(ds, name) || @warn "$name is not define"

const path = joinpath(@__DIR__, "example.nc")

# laod *.nc file to read the data
ds = cdiDataset(path)

# Print the created cdiDataset with all metadata 
print(ds)

# test if the variables exists
varID_1 = 1
#hasVar(ds, varID_1) # haskey is only available for strings
hasVar(ds, "varname2")

# create a buffer to load 'varname1'
var2 = Array{Float64}(undef, size(ds["varname2"]))

# loop over the time steps 
while getNextTimestep(ds)
    # get the verification date and time 
    @info "Step " * string(getTimestep(ds))
    @info DateTime(ds)

    # read 'varname1' and allocate storage
    # have acess via the varID
    var1 = loadData(ds[varID_1])
    # read 'varname2' into the pre allocated buffer
    loadData!(var2, ds["varname2"])
end
