# import the high-level libcdi wraper
using cdi.cdiDatasets

# import the c-libcdi Enums like TIME.VARYING and FileTypes.FILETYPE_NC
using cdi

# for logging only 
using Dates

@info "cdi_write example has started: " * string(Dates.now())

const lons = [0, 30, 60, 90, 120, 150, 180, 210, 240, 270, 300, 330]
const lats = [-75, -45, -15, 15, 45, 75]
const levs = [101300, 92500, 85000, 50000, 20000]
const nts = 3 # number of timestamps
const path = joinpath(@__DIR__, "example.nc")

# Init var1 and var2
const var1 = fill(1.1, length(lons) * length(lats))
const var2 = fill(2.2, length(lons) * length(lats) * length(levs))

# Create a regular lon/lat grid
grid = LonLat(lons, lats)

# Create a surface level Z-axis
zAxis1 = ZAxisSurface()

# Create a pressure level Z-axis
zAxis2 = ZAxisPressure(levs)

# Define the variables
v1 = Variable("varname1", grid, zAxis1, TIME.VARYING)
v2 = Variable("varname2", grid, zAxis2, TIME.VARYING)

# Create a Time axis
tAxis = TAxisAbsolut()

# create a list of all variable templates 
varTemplates = [v1, v2]

# Create cdiDataset
ds = cdiDataset(path, FileTypes.FILETYPE_NC, tAxis, varTemplates)

# Print the created cdiDataset with all metadata 
print(ds)

# Loop over the number of time steps
while getTimestep(ds) < nts

    # calc the date and time
    dt = DateTime(2000, 01, 01 + getTimestep(ds), 12, 00)

    # set the date and time for this timestep
    setNextTimestep!(ds, dt)

    # Write var1 and var2
    storeData!(ds["varname1"], var1)
    storeData!(ds["varname2"], var2)
end

@info "cdi_write example has stoped: " * string(Dates.now())