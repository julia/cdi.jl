# cdi.jl
cdi.jl is a Julia interface for the [libcdi](https://gitlab.dkrz.de/mpim-sw/libcdi) project from the [Max-Planck-Institute for Meteorology](https://www.mpg.de/155345/meteorologie).

It is currently available in two versions. In the **auto** version is the interface created by the [Clang C to Julia](https://github.com/JuliaInterop/Clang.jl) wrapper project. The **hand** version is hand written and optimized to Julia and the type system.

Both versions are currently only available in a not stable version. Warnings and errors are possible. The code is tested with Julia 1.4 and 1.5 on Linux Systems.

# Install
You can install the cdi.jl project in two simple steps. First install [libcdi](#install-libcdi) and than [cdi.jl](#install-cdi.jl)

## Install libcdi
It is required to have a dynamic libcdi library accessible via the $LD_LIBRARY_PATH. To install the libcdi binary please follow the instruction at the [libcdi](https://gitlab.dkrz.de/mpim-sw/libcdi) project side. The following instructions are only a Linux example:

1. Install the required libraries:
```bash
> sudo apt-get update
> sudo apt-get install automake libnetcdff-dev libeccodes-dev
``` 
2. Clone the libcdi project
```bash
> git clone https://gitlab.dkrz.de/mpim-sw/libcdi.git
``` 
3. Configure the libcdi project. To install libcdi to a different location use `--prefix=PREFIX`.
```bash
> cd libcdi
> ./autogen.sh
> ./configure --with-eccodes=yes --with-netcdf=yes
```
4. Build the project. To speed up the make process you can add `-jN` to the make statement with N the number of parallel execution instances. 
```bash
> make
> make check
> make install
```
5. If you used `--prefix=PREFIX` in step 3 you has to add `PREFIX/lib` to $LD_LIBRARY_PATH with:
```bash
> export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:PREFIX/lib 
```
6. To check the install process you can run the include example files. If both examples run without errors you can continue with [Install cdi.jl](#Install-cdi.jl)
```bash
> cd examples
> ./cdi_write
> ./cdi_read
```

## Install cdi.jl
cdi.jl is currently available in two versions. In the **auto** version is the interface created by the [Clang C to Julia](https://github.com/JuliaInterop/Clang.jl) wrapper project. The **hand** version is hand written and optimized to Julia and the type system.

To install the cdi.jl package, open Julia and press `]` to open the package manager. Now you can install cdi.jl by type in
```julia
add https://gitlab.dkrz.de/m300859/cdi.jl.git#VERSION
```
To use the **auto** version please replace `VERSION` with `auto`. If you want to test the **hand written** version please use `hand` instead of `VERSION`. To exit the package manager use the backspace key.

### change the cdi.jl version
To change the used version of cdi.jl please re-enter the package manager and follow the steps from [Install cdi.jl](#Install-cdi.jl) with the now selected version.

### update cdi.jl
cdi.jl is currently still in development. To update your local version of cdi.jl enter the package manager in Julia by typing `]` to the REPL. Now update cdi.jl by `update cdi`

# Examples
Both versions, the auto-generated version by Clang and the hand written version, has examples to test the interface. To use these examples you has to clone the git repo independent from the Julia package manager.
```bash
> git clone https://gitlab.dkrz.de/m300859/cdi.jl.git
> git checkout auto
> cd examples
> julia cdi_write.jl
```
The examples are depend by the selected version of cdi.jl. In case of an error please check that you use the correct cdi.jl version. To change the cdi.jl see [here](#update-cdi.jl).

# Requirements
Julia 1.4 or 1.5 on Linux

Please check the requirements for the **libcdi** project [here](https://gitlab.dkrz.de/mpim-sw/libcdi).
