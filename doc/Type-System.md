```plantuml
@startuml
hide methods
hide empty attributes

' File
class cdiDataset {
    String filename
    int streamID
    int vListID
}

cdiDataset "1" o-- "n" TAxis
cdiDataset "1" o-- "n" Grid
cdiDataset "1" o-- "n" ZAxis
cdiDataset "1" o-- "n" Variable


' Grid
abstract Class Grid
abstract class Grid2D
abstract class Grid1D

Grid <|-- Grid2D
Grid <|-- Grid1D

class LonLat {
    int gridID
    size_t Xsize
    size_t Ysize
    double[] Xvalues
    double[] Yvalues
}
class Gaussian {
    int gridID
    size_t Xsize
    size_t Ysize
    double[] Xvalues
    double[] Yvalues
}

class Spectral {
    int gridID
    size_t size
    double[] values
}

Grid2D <|-- LonLat
Grid2D <|-- Gaussian
Grid1D <|-- Spectral

' Time
abstract class TAxis

class Absolute
class Forcast
class Relative

TAxis <|-- Absolute
TAxis <|-- Forcast
TAxis <|-- Relative

' Z-Axis
abstract class ZAxis

class ZAxisPressure {
    String short_name
    String standard_name
    String long_name
    String unit
    uint size
    double[] values
}

class ZAxisSurface {
    String short_name
    String standard_name
    String long_name
    String unit
    uint size
    double[] values
}

ZAxis <|-- ZAxisPressure
ZAxis <|-- ZAxisSurface

' Variable
class Variable {
    String short_name
    String standard_name
    String long_name
    String unit
}

Variable "n" o-- "1" TAxis: time_axis
Variable "n" o-- "1" Grid: grid
Variable "n" o-- "1" ZAxis: z_axis
@endum
```