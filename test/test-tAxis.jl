import cdi.cdiDatasets: TAxisTemplate, AbstractTAxis
import cdi.TAXIS: ABSOLUTE, RELATIVE, FORECAST

@testset "t-axis" begin
    # construct TAxis
    @testset "construct TAxisAbsolut" begin
        t = TAxisAbsolut()
        @test typeof(t) == TAxisTemplate
        @test t.type == ABSOLUTE
        ds = cdiDataset(outputFilePath, FileTypes.FILETYPE_NC, t, dummyVariableList(1))
        @test typeof(tAxis(ds)) == TAxisAbsolut
    end
    @testset "construct TAxisRelativ" begin
        t = TAxisRelativ()
        @test typeof(t) == TAxisTemplate
        @test t.type == RELATIVE
        ds = cdiDataset(outputFilePath, FileTypes.FILETYPE_NC, t, dummyVariableList(1))
        @test typeof(tAxis(ds)) == TAxisRelativ
    end
    @testset "construct TAxisForecast" begin
        t = TAxisForecast()
        @test typeof(t) == TAxisTemplate
        @test t.type == FORECAST
        ds = cdiDataset(outputFilePath, FileTypes.FILETYPE_NC, t, dummyVariableList(1))
        @test typeof(tAxis(ds)) == TAxisForecast
    end

    @testset "getTimestep" begin
        ds = dummyDataset()

        @test getTimestep(ds) == 0
        getNextTimestep(ds)
        @test getTimestep(ds) == 1
        getNextTimestep(ds)
        @test getTimestep(ds) == 2

        @test resetTimestep(tAxis(ds)) == 0
        @test getTimestep(ds) == 0
        getNextTimestep(ds)
        @test getTimestep(ds) == 1
        getNextTimestep(ds)
        @test getTimestep(ds) == 2
    end

    # duplicate TAxis
    @testset "duplicate TAxis" begin
        dsIn = cdiDataset(inputFilePath)
        tOld = tAxis(dsIn)
        tNew = duplicateTAxis(tOld)
        @test tNew.blueprint === tOld
        dsOut = cdiDataset("test.nc", FileTypes.FILETYPE_NC, tNew, dummyVariableList(1))
        @test typeof(tAxis(dsIn)) == typeof(tAxis(dsOut))
    end

    # tAxis
    @test begin
        ds = dummyDataset()
        typeof(tAxis(ds)) <: AbstractTAxis
    end

    # show only compile test
    @test begin
        ds = dummyDataset()
        show(dummyIO, tAxis(ds))
        true # no exception
    end
end