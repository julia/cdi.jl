using Test

using cdi
using cdi.cdic
using cdi.cdiDatasets

@testset "cdic" begin
    @test cdiStringError(1) != ""
end

# include test files
include("helper.jl")
include("test-dataset.jl")
include("test-tAxis.jl")
include("test-grid.jl")
include("test-zAxis.jl")
include("test-variable.jl")