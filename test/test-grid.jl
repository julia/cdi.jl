import cdi.cdiDatasets: AbstractGrid, gridType

# not in use
macro gridTest(type)
    :(
        g = $type([0.], [0.]);
        @test g.gridID != 0;
        @test g.xSize == 1;
        @test g.ySize == 1;
        @test g.xValues == [0.];
        @test g.yValues == [0.];
        @test gridType(g) == gridType($type);
        @test gridType(gridType(g)) == $type;
        @test size(g) == (1, 1);
        @test length(g) == 1;
    )
end

@testset "grid" begin
     # construct TAxis
    types = [LonLat, Gaussian, GaussianReduced, Curvilinear]
    @testset "2D grid $type" for type in types
        g = type([0.], [0.]);
        @test g.gridID != 0;
        @test g.xSize == 1;
        @test g.ySize == 1;
        @test g.xValues == [0.];
        @test g.yValues == [0.];
        @test gridType(gridType(g)) == type;
        @test size(g) == (1, 1);
        @test length(g) == 1;
    end
    @testset "construct 2D grid" begin
        lon = collect(Float64, -180:30:180)
        lat = collect(Float64, 0:30:180)
        g = LonLat(lon, lat)
        @test g.gridID != 0
        @test g.xSize == length(lon)
        @test g.ySize == length(lat)
        @test g.xValues == lon
        @test g.yValues == lat
        @test size(g) == (length(lon), length(lat))
        @test length(g) == length(lon) * length(lat)
    end

    # grid type
    @test begin
        ds = dummyDataset()
        gridType(gridType(grid(ds["varname1"]))) == LonLat
    end

    # grid
    @test begin
        ds = dummyDataset()
        typeof(grid(ds["varname1"])) <: AbstractGrid
    end

    # show only compile test
    @test begin
        ds = dummyDataset()
        show(dummyIO, grid(ds["varname1"]))
        true # no exception
    end
end