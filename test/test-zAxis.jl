import cdi.cdiDatasets: AbstractZAxis, AbstractZAxisOneLayer, AbstractZAxisMultiLayer, AbstractZAxisHybridLayer, zAxisType
import ..ZAXIS: SURFACE, PRESSURE

function constructDummy(t::Type)
    @assert t <: AbstractZAxis
    
    if t <: AbstractZAxisOneLayer
        return t()
    elseif t <: AbstractZAxisMultiLayer
        levels = [1,2,3,4,5,6]
        return t(levels)
    elseif t <: AbstractZAxisHybridLayer
        levels = [1,2,3,4,5,6]
        lowerBound = [1,2,3,4,5,6]
        upperBound = [2,3,4,5,6,-1]
        vct = [0, 6000, 10000, 16000, 8000, 0, 0, 0.0004, 0.03, 0.2, 0.7, 1.0]

        # construct the type also with the simple constructor
        dummy = t(levels, vct);

        return t(levels, vct, lowerBound, upperBound)
    end
end

@testset "z-axis" begin
    axisOneLayer = [
        ZAxisSurface, ZAxisMeanSeaLevel, ZAxisTopOfAtmosphere,
        ZAxisSeaBottom, ZAxisAtmosphere, ZAxisCloudBase, ZAxisCloudTop,
        ZAxisIsothermZero, ZAxisSnow, ZAxisLakeBottom, ZAxisSedimentBottom,
        ZAxisSedimentBottomTA, ZAxisSedimentBottomTW
    ]
    axisMultiLayer = [
        ZAxisPressure, ZAxisHeight, ZAxisDepthBelowSea, ZAxisDepthBelowLand, 
        ZAxisIsentropic, ZAxisAltitude
    ]
    axisHybridLayer = [
        ZAxisHybrid, ZAxisHybridHalf
    ]
    allAxes = [axisOneLayer..., axisMultiLayer..., axisHybridLayer...]

    # construct z-axis
    @testset "construct z-axis: $type" for type in allAxes
        z = constructDummy(type)
        @test z.axisID != 0;
        @test zAxisType(zAxisType(z)) == typeof(z)
    end

    # length() and size()
    @testset "length and size z-axis: $type" for type in axisOneLayer
        z = constructDummy(type)
        @test length(z) == 1 # see constructDummy
        @test size(z) == (1,)
    end
    @testset "length and size z-axis: $type" for type in [axisMultiLayer..., axisHybridLayer...]
        z = constructDummy(type)
        @test length(z) == 6 # see constructDummy
        @test size(z) == (6,)
    end

    # get unit
    @testset "get unit z-axis: $type" for type in allAxes
        z = constructDummy(type)
        @test typeof(unit(z)) <: String
    end

    # get levels
    @testset "length and size z-axis: $type" for type in [axisMultiLayer..., axisHybridLayer...]
        z = constructDummy(type)
        @test levels(z) == [1,2,3,4,5,6] # see constructDummy
    end

    # get hyam and hybm
    @testset "get hyam and hybm: $type" for type in axisHybridLayer
        z = constructDummy(type)
        @test hyam(z) == [3000, 8000, 13000, 12000, 4000, 0]
        @test hybm(z) == [0.0, 0.0002, 0.0152, 0.115, 0.44999999999999996, 0.85] 
    end

    # z-axis type
    @test begin
        ds = dummyDataset()
        zAxisType(zAxis(ds["varname1"])) == SURFACE
    end
    @test begin
        ds = dummyDataset()
        zAxisType(zAxis(ds["varname2"])) == PRESSURE
    end

    # z-axis
    @test begin
        ds = dummyDataset()
        typeof(zAxis(ds["varname1"])) <: AbstractZAxis
    end

    # show only compile test
    @test begin
        ds = dummyDataset()
        show(dummyIO, zAxis(ds["varname1"]))
        true # no exception
    end
    @testset "show z-axis: $type" for type in allAxes
        z = constructDummy(type)
        show(dummyIO, z)
        true # no exception
    end
end