import Base: OneTo

const inputFilePath = joinpath(@__DIR__, "input.nc")
const outputFilePath = joinpath(@__DIR__, "output.nc")
const dummyIO = open(joinpath(@__DIR__, "io.txt"), "w");

dummyDataset() = cdiDataset(inputFilePath)

function dummyLonLat(lons=[0.], lats=[0.])
    return LonLat(lons, lats)
end

dummyGrid() = dummyLonLat()

dummyZAxis() = ZAxisSurface()

dummyTAxis() = TAxisAbsolut()

function dummyVariable(;name="dummyVar", grid=dummyGrid(), zAxis=dummyZAxis(), t=TIME.VARYING)
    Variable(name, grid, zAxis, t)
end

function dummyVariableList(count; prefix="dummyVar", grid=dummyGrid(), zAxis=dummyZAxis(), t=TIME.VARYING)
    out = Vector{VariableTemplate}(undef, 0)
    for i in OneTo(count)
        push!(out, Variable(prefix * string(i), grid, zAxis, t))
    end
    return out
end
