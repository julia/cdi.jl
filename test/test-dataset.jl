@testset "cdiDataset" begin
    # constructors
    @test begin
        ds = cdiDataset(inputFilePath)
        typeof(ds) == cdiDataset
    end
    @test begin
        ds = cdiDataset(outputFilePath, FileTypes.FILETYPE_NC, dummyTAxis(), dummyVariableList(1))
        typeof(ds) == cdiDataset
    end
    
    # getIndex
    @test begin
        ds = dummyDataset()
        typeof(ds["varname1"]) == Variable
    end
    @test begin
        ds = dummyDataset()
        varID = 1
        typeof(ds[varID]) == Variable
    end
    @test_throws BoundsError begin
        ds = dummyDataset()
        varID = 10 # not existing key
        typeof(ds[varID])
    end
    @test_throws BoundsError begin
        ds = dummyDataset()
        typeof(ds["xxx"])
    end
end