import cdi.cdiDatasets: AbstractGrid, AbstractZAxis, AbstractTAxis, VariableTemplate


@testset "variables" begin

    @testset "laod variable meta data" begin
        ds = dummyDataset()

        var1 = ds["varname1"]
        @test var1.varID == 0
        @test var1.stdName == ""
        @test var1.name == "varname1"
        @test var1.longName == ""
        @test var1.unit == ""
        @test var1.code == -1
        @test typeof(grid(var1)) <: AbstractGrid
        @test typeof(zAxis(var1)) <: AbstractZAxis
        @test typeof(tAxis(var1)) <: AbstractTAxis

        var2_id = 1
        var1 = ds[var2_id]
        @test var1.varID == 1
        @test var1.stdName == ""
        @test var1.name == "varname2"
        @test var1.longName == ""
        @test var1.unit == ""
        @test var1.code == -2
        @test typeof(grid(var1)) <: AbstractGrid
        @test typeof(zAxis(var1)) <: AbstractZAxis
        @test typeof(tAxis(var1)) <: AbstractTAxis
    end

    @testset "variable size" begin
        ds = dummyDataset()

        # the vars use different z-axis
        @test size(ds["varname1"]) == (12,6,1)
        @test size(ds["varname2"]) == (12,6,5)
    end

    @testset "variable length" begin
        ds = dummyDataset()

        @test length(ds["varname1"]) == 72
        @test length(ds["varname2"]) == 360
    end

    @testset "get variable unit" begin
        ds = dummyDataset()

        @test unit(ds["varname1"]) == ""
        @test unit(ds["varname2"]) == ""

        var1 = Variable("testVar1", dummyGrid(), dummyZAxis(), TIME.VARYING)
        @test unit(var1) == ""

        var2 = Variable("testVar2", dummyGrid(), dummyZAxis(), TIME.VARYING; unit="m/s")
        @test unit(var2) == "m/s"
    end

    @testset "get variable z-axis" begin
        ds = dummyDataset()

        @test getZAxis(ds["varname1"]) == ds.zAxis[1]
        @test getZAxis(ds["varname2"]) == ds.zAxis[2]

        zAxis = dummyZAxis()
        var1 = Variable("testVar", dummyGrid(), zAxis, TIME.VARYING)
        @test getZAxis(var1) == zAxis
    end

    @testset "get variable grid" begin
        ds = dummyDataset()

        @test getGrid(ds["varname1"]) == ds.grids[1]
        @test getGrid(ds["varname2"]) == ds.grids[1]

        grid = dummyGrid()
        var1 = Variable("testVar", grid, dummyZAxis(), TIME.VARYING)
        @test getGrid(var1) == grid
    end

    @testset "create simple variable" begin
        grid = dummyGrid()
        zAxis = dummyZAxis()
        name = "testName1"
        var = Variable(name, grid, zAxis, TIME.VARYING)

        @test typeof(var) <: VariableTemplate
        @test var.name == name
        @test var.stdName == name
        @test var.grid == grid
        @test var.zAxis == zAxis
        @test var.time == TIME.VARYING
    end

    @testset "create advanced variable" begin
        grid = dummyGrid()
        zAxis = dummyZAxis()
        name = "testName1"
        stdName = "stdName1"
        longName = "longName1"
        unit = "m/s"
        code = 42
        missValue = 42.42
        var = Variable(name, grid, zAxis, TIME.VARYING;
            stdName, longName, unit, code, missValue)

        @test typeof(var) <: VariableTemplate
        @test var.name == name
        @test var.grid == grid
        @test var.zAxis == zAxis
        @test var.time == TIME.VARYING
        @test var.stdName == stdName
        @test var.longName == longName
        @test var.unit == unit
        @test var.code == code
        @test var.missValue == missValue
    end

    @testset "load variable data" begin
        ds = cdiDataset(inputFilePath)

        @test !isempty(loadData(ds["varname1"]))
        @test !isempty(loadData(ds["varname2"]))
    end

    @testset "load variable data inplace" begin
        ds = cdiDataset(inputFilePath)

        data1 = Array{Float64}(undef, size(ds["varname1"]))
        data2 = Array{Float64}(undef, size(ds["varname2"]))

        loadData!(data1, ds["varname1"])
        loadData!(data2, ds["varname2"])

        @test !isempty(data1)
        @test !isempty(data2)
    end

    @testset "store variable data" begin
        grid = dummyGrid()
        zAxis = dummyZAxis()
        tAxis = dummyTAxis()

        var1 = Variable("var", grid, zAxis, TIME.VARYING)
        ds = cdiDataset(outputFilePath, FileTypes.FILETYPE_NC, tAxis, [var1])

        data = fill(1.0, size(var1))

        # write data
        dt = DateTime(2000, 01, 01 + getTimestep(ds), 12, 00)
        setNextTimestep!(ds, dt)
        storeData!(ds["var"], data)

        close(ds)

        ds = cdiDataset(outputFilePath)
        @test loadData(ds["var"])[1,1,1] == data[1,1,1]
        true
    end

    @testset "get not existing variable" begin
        ds = dummyDataset()

        @test_throws BoundsError ds["not_exsist"]
        @test_throws BoundsError ds[42]
    end

    # show only compile test
    @test begin 
        ds = dummyDataset()
        var2 = 1
        show(dummyIO, ds["varname1"])
        show(dummyIO, ds[var2])
        true
    end
end