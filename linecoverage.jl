using Coverage

# process '*.cov' files
coverage = process_folder() # defaults to src/; alternatively, supply the folder name as argument

# process '*.info' files
coverage = merge_coverage_counts(coverage, filter!(
    let prefixes = (joinpath(pwd(), "src", ""))
        c -> any(p -> startswith(c.filename, p), prefixes)
    end,
    LCOV.readfolder("test")))

# Get total coverage for all Julia files
covered_lines, total_lines = get_summary(coverage)

println("")
println("")
println("")

@info "Total lines " * string(total_lines)
@info "Covered lines " * string(covered_lines) * "  =>  " * string(floor(Int, 100/total_lines*covered_lines)) * "%"
