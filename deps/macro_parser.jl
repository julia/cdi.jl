module MacroParser

using JSON

####### local Types ######

@enum ReadState begin
    waitUntilEnum
    readEnumValues
end

struct rawEnumValue
    name::String
    value::String
    comment::String
end

mutable struct rawEnum 
    name::String
    values::Vector{rawEnumValue}
end

function rawEnum()
    rawEnum("", Vector{rawEnumValue}(undef, 0))
end

####### local functions ########
function readEnumValue(tokens::Vector{String})::rawEnumValue
    if length(tokens) < 3 || tokens[1] != "#define"
        return
    end

    comment = join(tokens[5:end], " ")

    return rawEnumValue(tokens[2], tokens[3], comment)
end

function macroName(name::AbstractString)
    nameparts = split(name, "_")
    res = name
    if length(nameparts) == 2
        res = nameparts[2]
    elseif length(nameparts) > 2
        res = join(nameparts[2:end], "_")
    end

    if length(res) > 0 && isdigit(res[1])
        res = '_' * res
    end

    return res
end

###### Public Function #######

function run(libcdiPath, exportPath)
    println("Start converting Macro to Enum")
    INPUTPATH = libcdiPath * "/src/cdi.h" |> normpath
    OUTPUTPATH = exportPath * "/src/cdic/cdi_macro.jl" |> normpath
    BASEDIR = @__DIR__
    BLACKLIST = BASEDIR * "/blacklist.json"

    # Read input file
    io = open(INPUTPATH, "r")
    lines = split(read(io, String), '\n') 
    close(io)

    enums = Vector{rawEnum}(undef, 0)
    readState = waitUntilEnum
    tmpEnum = rawEnum()
    
    for line in lines
        trimedline = line |> lstrip |> rstrip

        # skip empty lines
        if length(trimedline) == 0
            continue
        end

        # create clean token stream from line
        tokens = filter(x -> !isempty(x), split(trimedline, ' '))

        if tokens[1] == "//" && tokens[2] == "#BEGIN#" && readState == waitUntilEnum
            tmpEnum.name = tokens[3]
            readState = readEnumValues
        elseif tokens[1] == "//" && tokens[2] == "#BEGIN#" && readState == readEnumValues
            println("Sytax Error open enum block twice")
            println("Error at line: \"$line\"")
            return
        elseif readState == readEnumValues && tokens[1] == "#define"
            append!(tmpEnum.values, [readEnumValue(convert(Vector{String}, tokens))])
        elseif tokens[1] == "//" && tokens[2] == "#END#" && readState == waitUntilEnum
            println("Sytax Error close enum block without open one")
            println("Error at line: \"$line\"")
            return
        elseif readState == readEnumValues && tokens[1] == "//" && tokens[2] == "#END#"
            append!(enums, [tmpEnum])
            readState = waitUntilEnum
            tmpEnum = rawEnum()
        end
    end

    if length(enums) == 0
        println("No macros found")
        exit(0)
    end

    println("load macro Blacklist from $BLACKLIST")
    blacklist = Dict()
    open(BLACKLIST, "r") do f
        blacklist = JSON.parse(f)
    end

    println("open output file to write $OUTPUTPATH")

    io = open(OUTPUTPATH, "w")
    for e in enums
        modulename = e.name
        enumname = e.name * "_t"

        enum_blacklist = get(blacklist, enumname, Vector{Any}(undef, 0))

        # create namespace
        write(io, "module $modulename\n")

        #create enum
        write(io, "@enum $enumname::Cint begin\n")
        for v in (e.values)
            name = macroName(v.name)

            # ignore define values from the black list
            if name in enum_blacklist
                continue
            end

            if v.comment == ""
                write(io, "    $name = $(v.value)\n")
            else
                write(io, "    $name = $(v.value) # $(v.comment)\n")
            end
        end
        write(io, "end # End of Enum $enumname\n")
        
        #export enum to namespace
        write(io, "export $enumname\n")

        write(io, "end # End of Module $modulename\n")
        write(io, "using .$modulename")
        write(io, "\n\n")
    end

    close(io)

    println("Finished converting")
    println("Save file to $OUTPUTPATH")
end # Function run()

export run
end # Module MacroParser