
# clone the libcdi repo
const BASEPATH = @__DIR__
const CDIPATH = BASEPATH * "/../" |> normpath
const CLONEPATH = CDIPATH * "libcdi" |> normpath

cmd_clone = `git clone https://gitlab.dkrz.de/mpim-sw/libcdi.git $CLONEPATH`
cmd_checkout = `git checkout cdi_julia`
cmd_pull = `git pull --force`

if ~isdir(CLONEPATH)
    run(cmd_clone)
end
const OLD_PATH = pwd()
cd(CLONEPATH)
run(cmd_checkout)
run(cmd_pull)
cd(OLD_PATH)

include("macro_parser.jl")
MacroParser.run(CLONEPATH, CDIPATH)