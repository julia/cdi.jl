#!/bin/bash

# clean old files
rm src/cdiDataset/*.cov &> /dev/null
rm src/cdic/*.cov &> /dev/null
rm test/*.cov &> /dev/null
rm *.info &> /dev/null


julia --project=. --code-coverage=user -e "import Pkg; Pkg.test(;coverage=true)"
julia --project=. --code-coverage=tracefile-%p.info --code-coverage=user -e "import Pkg; Pkg.test(;coverage=true)"

julia -e "import Pkg; Pkg.add(\"Coverage\"); include(\"linecoverage.jl\")"