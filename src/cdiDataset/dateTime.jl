import Dates.Date, Dates.Time, Dates.DateTime

function Date(ds::cdiDataset)::Date
    date = taxisInqVdate(ds.tAxis.axisID)
    return cdiDecodeDate(date)
end
export Date

function Time(ds::cdiDataset)::Time
    time = taxisInqVtime(ds.tAxis.axisID)
    return cdiDecodeTime(time)
end
export Time

function DateTime(ds::cdiDataset)::DateTime
    return DateTime(Date(ds), Time(ds))
end
export DateTime