import Base.size, Base.length
import Base.show

import ..GRID: GRID_t, GAUSSIAN, GAUSSIAN_REDUCED, LONLAT, SPECTRAL, CURVILINEAR

if !isdefined(@__MODULE__, :ismatch)
    using Match
end

############## Types ##############
abstract type AbstractGrid end
abstract type AbstractGrid1D <: AbstractGrid end
abstract type AbstractGrid2D <: AbstractGrid end

"""
declare an `AbstractGrid1D` type. Additional define the constructor for this type end export
 the sybole
## Parameter:
- name: declare the name of the generated type and the constructor
- key: is a `GRID_t` value and will be used in the construction process at runtime
## Note:
- For full support add the `name` and `key` in the `gridType(gid::GRID_t)` function!
- You have to add the `key` in the imports as well
"""
macro grid1D(name, key)
    quote
        struct $(esc(name)) <: AbstractGrid2D
            gridID::Cint
            size::Csize_t
        end
        $(esc(name))(size::Integer) = Grid2D(size, $(esc(key)))
        export $(esc(name))
    end
end

"""
declare an `AbstractGrid2D` type. Additional define the constructor for this type end export
 the sybole
## Parameter:
- name: declare the name of the generated type and the constructor
- key: is a `GRID_t` value and will be used in the construction process at runtime
## Note:
- For full support add the `name` and `key` in the `gridType(gid::GRID_t)` function!
- You have to add the `key` in the imports as well
"""
macro grid2D(name, key)
    quote
        struct $(esc(name)) <: AbstractGrid2D
            gridID::Cint
            xSize::Csize_t
            ySize::Csize_t
            xValues::Vector{Cdouble}
            yValues::Vector{Cdouble}
        end
        $(esc(name))(lons::AbstractVector, lats::AbstractVector) = Grid2D(lons, lats, $(esc(key)))
        export $(esc(name))
    end
end


@grid2D LonLat LONLAT

@grid2D Gaussian GAUSSIAN

@grid2D GaussianReduced GAUSSIAN_REDUCED

@grid1D Spectral SPECTRAL

@grid2D Curvilinear CURVILINEAR

############## public functions ##############
Base.size(grid::AbstractGrid1D) =  Int64(grid.size)
Base.size(grid::AbstractGrid2D) = Int64(grid.xSize), Int64(grid.ySize)
export size

Base.length(grid::AbstractGrid1D) =  grid.size
Base.length(grid::AbstractGrid2D) = grid.xSize * grid.ySize
export length

function Base.show(io::IO, g::AbstractGrid2D; indent="")
    printstyled(io, indent, "Grid: ", g.gridID, color=:light_green)
    printstyled(io, " (", gridType(g), ")", "\n", color=:blue)

    print(io, indent, "   ", "grid X-size: ", g.xSize, "\n")
    print(io, indent, "   ", "grid Y-size: ", g.ySize, "\n")
end

############## Helper functions ##############

"""
construct an 2D grid
"""
function Grid2D(xVals::AbstractVector, yVals::AbstractVector, t::GRID_t)::AbstractGrid2D
    T = gridType(t)
    @assert T <: AbstractGrid2D

    gridID = gridCreate(t, length(xVals) * length(yVals))
    gridDefXsize(gridID, length(xVals))
    gridDefYsize(gridID, length(yVals))
    gridDefXvals(gridID, xVals)
    gridDefYvals(gridID, yVals)

    return T(gridID, length(xVals), length(yVals), xVals, yVals)
end

"""
construct an 1D grid
"""
function Grid1D(size::Integer, t::GRID_t)::AbstractGrid1D
    T = gridType(t)
    @assert T <: AbstractGrid1D

    # TODO Is this correct, ask Uwe?
    gridID = gridCreate(t, (size + 1) * (size + 2))
    gridDefTrunc(gridID, size)
    gridDefComplexPacking(gridID, 1)

    return T(gridID, size)
end

"""
construct an 2d grid from file by gridID and grid type
"""
function loadGrid2D(gridID::Cint, T::Type)::T
    @assert T <: AbstractGrid2D

    xSize = gridInqXsize(gridID)
    ySize = gridInqYsize(gridID)
    xValues = gridInqXvals(gridID)
    yValues = gridInqYvals(gridID)

    return T(gridID, xSize, ySize, xValues, yValues)
end

# !!! not supported yet !!!
function loadGrid1D(gridID::Cint, T::Type)::T
    @assert T <: AbstractGrid1D

    size = gridInqSize(gridID)

    return T(gridID, size)
end

# only for internal stuff
struct ErrorGrid <: AbstractGrid end

"""
load a grid from file by gridID
"""
function loadGrid(gridID::Cint)::AbstractGrid
    @debug "load grid"
    type = gridType(gridInqType(gridID))

    if type == ErrorGrid
        # TODO throw exception
        @error "Grid type " * string(gridInqType(gridID)) * " is not supported yed"
        return nothing
    elseif type <: AbstractGrid2D
        return loadGrid2D(gridID, type)
    else 
        return loadGrid1D(gridID, type)
    end

    return g
end

"""
return the grid type enum value
"""
gridType(g::AbstractGrid) = GRID_t( gridInqType(g.gridID))

"""
return the julia grid Type like `LonLat <: AbstractGrid2D` from integer.
Integer must in the range of the enum cdic.GRID.GRID_t
"""
gridType(g::Integer) = gridType(GRID_t(g))

"""
convert the "c cdi" enum value to julia type like `LonLat <: AbstractGrid2D`
"""
gridType(g::GRID_t) = @match Int(g) begin
    2  => Gaussian
    3  => GaussianReduced
    4  => LonLat
    5  => Spectral
    10 => Curvilinear
    _ => ErrorGrid
end