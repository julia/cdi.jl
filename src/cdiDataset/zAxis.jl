import Base.size, Base.length
import Base.show
import Base.OneTo

import ..ZAXIS: ZAXIS_t, SURFACE, PRESSURE, HEIGHT, DEPTH_BELOW_SEA, DEPTH_BELOW_LAND
import ..ZAXIS: ISENTROPIC, ALTITUDE, MEANSEA, TOA, SEA_BOTTOM, ATMOSPHERE, CLOUD_BASE
import ..ZAXIS: CLOUD_TOP, ISOTHERM_ZERO, SNOW, LAKE_BOTTOM, SEDIMENT_BOTTOM, SEDIMENT_BOTTOM_TA
import ..ZAXIS: SEDIMENT_BOTTOM_TW, HYBRID_HALF, HYBRID

if !isdefined(@__MODULE__, :ismatch)
    using Match
end

############## Types ##############
abstract type AbstractZAxis end
abstract type AbstractZAxis1D <: AbstractZAxis end

abstract type AbstractZAxisOneLayer <: AbstractZAxis1D end
abstract type AbstractZAxisMultiLayer <: AbstractZAxis1D end

abstract type AbstractZAxisHybridLayer <: AbstractZAxis end

"""
declare an `AbstractZAxisOneLayer` type. Additional define the constructor for this type end export
 the sybole
## Parameter:
- name: declare the name of the generated type and the constructor
- key: is a `ZAXIS_t` value and will be used in the construction process at runtime
## Note:
- For full support add the `name` and `key` in the `zAxisType(z::ZAXIS_t)` function!
- You have to add the `key` in the imports as well
"""
macro ZAxisOneLayer(name, key)
    quote
        # declare the struct
        struct $(esc(name)) <: AbstractZAxisOneLayer
            axisID::Cint
            name::String
            std_name::String
            long_name::String
            unit::String
        end
        # define constructor
        $(esc(name))() = ZAxis($(esc(key)))
        # export symbol
        export $(esc(name))
    end
end

"""
declare an `AbstractZAxisMultiLayer` type. Additional define the constructor for this type end export
 the sybole
## Parameter:
- name: declare the name of the generated type and the constructor
- key: is a `ZAXIS_t` value and will be used in the construction process at runtime
## Note:
- For full support add the `name` and `key` in the `zAxisType(z::ZAXIS_t)` function!
- You have to add the `key` in the imports as well
"""
macro ZAxisMultiLayer(name, key)
    quote
        # declare the struct
        struct $(esc(name)) <: AbstractZAxisMultiLayer
            axisID::Cint
            name::String
            std_name::String
            long_name::String
            unit::String
            levels::Vector{Cdouble}
        end
        # define constructor
        $(esc(name))(levels::AbstractVector) = ZAxis(levels, $(esc(key)))
        # export symbol
        export $(esc(name))
    end
end

"""
declare an `AbstractZAxisHybrid` type. Additional define the constructor for this type end export
 the sybole
## Parameter:
- name: declare the name of the generated type and the constructor
- key: is a `ZAXIS_t` value and will be used in the construction process at runtime
## Note:
- For full support add the `name` and `key` in the `zAxisType(z::ZAXIS_t)` function!
- You have to add the `key` in the imports as well
"""
macro ZAxisHybridLayer(name, key)
    quote
        # declare the struct
        struct $(esc(name)) <: AbstractZAxisHybridLayer
            axisID::Cint
            name::String
            std_name::String
            long_name::String
            unit::String
            levels::Vector{Cdouble}
            vct::Vector{Cdouble}
            lowerBound::Vector{Cdouble}
            upperBound::Vector{Cdouble}
            hyam::Vector{Cdouble}
            hybm::Vector{Cdouble}
        end
        # define constructor
        $(esc(name))(levels::AbstractVector, vct::AbstractVector) = ZAxis(levels, vct, $(esc(key)))
        $(esc(name))(levels::AbstractVector, vct::AbstractVector, lowerBound::AbstractVector, upperBound::AbstractVector) = ZAxis(levels, vct, lowerBound, upperBound, $(esc(key)))
        # export symbol
        export $(esc(name))
    end
end

@ZAxisOneLayer ZAxisSurface SURFACE

@ZAxisHybridLayer ZAxisHybrid HYBRID

@ZAxisHybridLayer ZAxisHybridHalf HYBRID_HALF

@ZAxisMultiLayer ZAxisPressure PRESSURE

@ZAxisMultiLayer ZAxisHeight  HEIGHT

@ZAxisMultiLayer ZAxisDepthBelowSea DEPTH_BELOW_SEA

@ZAxisMultiLayer ZAxisDepthBelowLand DEPTH_BELOW_LAND

@ZAxisMultiLayer ZAxisIsentropic ISENTROPIC

@ZAxisMultiLayer ZAxisAltitude ALTITUDE

@ZAxisOneLayer ZAxisMeanSeaLevel MEANSEA

@ZAxisOneLayer ZAxisTopOfAtmosphere TOA

@ZAxisOneLayer ZAxisSeaBottom SEA_BOTTOM

@ZAxisOneLayer ZAxisAtmosphere ATMOSPHERE

@ZAxisOneLayer ZAxisCloudBase CLOUD_BASE

@ZAxisOneLayer ZAxisCloudTop CLOUD_TOP

@ZAxisOneLayer ZAxisIsothermZero ISOTHERM_ZERO

@ZAxisOneLayer ZAxisSnow SNOW

@ZAxisOneLayer ZAxisLakeBottom LAKE_BOTTOM

@ZAxisOneLayer ZAxisSedimentBottom SEDIMENT_BOTTOM
@ZAxisOneLayer ZAxisSedimentBottomTA SEDIMENT_BOTTOM_TA
@ZAxisOneLayer ZAxisSedimentBottomTW SEDIMENT_BOTTOM_TW

############## public functions ##############
Base.size(axis::AbstractZAxisMultiLayer) = size(axis.levels)
Base.size(axis::AbstractZAxisHybridLayer) = size(axis.levels)
Base.size(axis::AbstractZAxisOneLayer) = (1,)
export size

Base.length(axis::AbstractZAxisMultiLayer) = length(axis.levels)
Base.length(axis::AbstractZAxisHybridLayer) = length(axis.levels)
Base.length(axis::AbstractZAxisOneLayer) = 1
export length

function Base.show(io::IO, z::AbstractZAxis; indent="")
    printstyled(io, indent, "Z-Axis: ", z.axisID, color=:light_green)
    printstyled(io, " (", zAxisType(z), ")", "\n", color=:blue)
    print(io, indent, "   ", "name:   ", z.name, "\n")
    print(io, indent, "   ", "unit:   ", z.unit, "\n")
    print(io, indent, "   ", "levels: ", length(z), "\n")
end
export show

"""
    unit(axis)

Return the z-axis unit as string
"""
unit(axis::AbstractZAxis) = axis.unit
export unit

"""
    levels(axis)

Return the an array of levels
"""
levels(axis::AbstractZAxisMultiLayer) = axis.levels
levels(axis::AbstractZAxisHybridLayer) = axis.levels
export levels

hyam(axis::AbstractZAxisHybridLayer) = axis.hyam
hybm(axis::AbstractZAxisHybridLayer) = axis.hybm
export hyam, hybm

############## Helper functions ##############
"""
construct an one layer z-axis
"""
function ZAxis(t::ZAXIS_t)::AbstractZAxisOneLayer
    T = zAxisType(t)
    @assert T <: AbstractZAxisOneLayer

    axisID = zaxisCreate(t, 1)

    return loadZAxis(axisID)
end

"""
construct an multy layer z-axis
"""
function ZAxis(levels::AbstractVector, t::ZAXIS_t)::AbstractZAxisMultiLayer
    T = zAxisType(t)
    @assert T <: AbstractZAxisMultiLayer

    axisID = zaxisCreate(t, length(levels))
    zaxisDefLevels(axisID, levels)

    return loadZAxis(axisID)
end

"""
construct an hybrid layer z-axis
"""
function ZAxis(levels::AbstractVector, vct::AbstractVector, t::ZAXIS_t)::AbstractZAxisHybridLayer
    T = zAxisType(t)
    @assert T <: AbstractZAxisHybridLayer
    @assert 2*length(levels) == length(vct)

    axisID = zaxisCreate(t, length(levels))
    zaxisDefLevels(axisID, levels)
    zaxisDefVct(axisID, vct);

    return loadZAxis(axisID)
end

"""
construct an hybrid layer z-axis
"""
function ZAxis(levels::AbstractVector, vct::AbstractVector, 
    lowerBound::AbstractVector, upperBound::AbstractVector, t::ZAXIS_t)::AbstractZAxisHybridLayer
    
    T = zAxisType(t)
    @assert T <: AbstractZAxisHybridLayer
    @assert 2*length(levels) == length(vct)
    @assert length(lowerBound) == length(upperBound)

    axisID = zaxisCreate(t, length(levels))
    zaxisDefLevels(axisID, levels)
    zaxisDefVct(axisID, vct);
    zaxisDefLbounds(axisID, lowerBound);
    zaxisDefUbounds(axisID, upperBound);

    return loadZAxis(axisID)
end

struct ZAxisError <: AbstractZAxis end

function loadZAxis(zID::Cint)::AbstractZAxis
    levels = zaxisInqLevels(zID)
    unit = zaxisInqUnits(zID)
    name = zaxisInqName(zID)
    long_name = zaxisInqLongname(zID)
    std_name = "" # is not supported by cdi yet

    z =  zAxisType(zaxisInqType(zID))

    if z === ZAxisError()
        # TODO throw exception
        @error "Z-Axis type " * string(zaxisInqType(zID)) * " is not supported yet"
    end

    @debug "load z-axis: " * string(name)

    if z <: AbstractZAxisOneLayer
        return z(zID, name, std_name, long_name, unit)
    elseif z <: AbstractZAxisMultiLayer
        return z(zID, name, std_name, long_name, unit, levels)
    elseif z <: AbstractZAxisHybridLayer
        lowerBound = zaxisInqLbounds(zID)
        upperBound = zaxisInqUbounds(zID)
        vct = zaxisInqVct(zID)

        hyam = Vector{Float64}(undef, length(levels))
        for i in OneTo(length(hyam))
            hyam[i] = (vct[i]+vct[i+1])/2
        end

        hybm = Vector{Float64}(undef, length(levels))
        for i in OneTo(length(hybm))
            offset = length(levels) - 1
            hybm[i] = (vct[offset+i]+vct[offset+i+1])/2
        end

        return z(zID, name, std_name, long_name, unit, levels, vct, lowerBound, upperBound, hyam, hybm)
    end
end

"""
return the grid type enum value
"""
zAxisType(z::AbstractZAxis) = ZAXIS_t(zaxisInqType(z.axisID))

"""
return the julia grid Type like `LonLat <: AbstractGrid2D` from integer.
Integer must in the range of the enum cdic.GRID.GRID_t
"""
zAxisType(z::Integer) = zAxisType(ZAXIS_t(z))

"""
convert the "c cdi" enum value to julia type like `LonLat <: AbstractGrid2D`
"""
zAxisType(z::ZAXIS_t) = @match Int(z) begin
    0  => ZAxisSurface
    # 1 => GENERIC
    2 => ZAxisHybrid
    3 => ZAxisHybridHalf
    4  => ZAxisPressure
    5  => ZAxisHeight
    6  => ZAxisDepthBelowSea
    7  => ZAxisDepthBelowLand
    8  => ZAxisIsentropic
    # 9 => TRAJECTORY
    10 => ZAxisAltitude
    # 11 => SIGMA
    12 => ZAxisMeanSeaLevel
    13 => ZAxisTopOfAtmosphere
    14 => ZAxisSeaBottom
    15 => ZAxisAtmosphere
    16 => ZAxisCloudBase
    17 => ZAxisCloudTop
    18 => ZAxisIsothermZero
    19 => ZAxisSnow
    20 => ZAxisLakeBottom
    21 => ZAxisSedimentBottom
    22 => ZAxisSedimentBottomTA
    23 => ZAxisSedimentBottomTW
    _ => ZAxisError
end