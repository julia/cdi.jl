import Base.show, Base.close
import Base.in, Base.keys, Base.haskey
import Base.getindex

mutable struct cdiDataset
    filename::AbstractString
    streamID::Cint
    vListID::Cint
    vars::Vector{Variable}
    grids::Vector{AbstractGrid}
    zAxis::Vector{AbstractZAxis}
    tAxis::AbstractTAxis
    varCacheName::Dict{AbstractString, Variable}
    varCacheID::Dict{Int64, Variable}

    function cdiDataset(filename::AbstractString, streamID::Cint, vListID::Cint)
        streamID < 0 && @error "StreamID is not >= 0"

        function _finalize(ds)
            @debug "destruct cdiDataset"
            if ds.streamID >= 0
                close(ds)
            end
        end

        # create objects
        ds = new()
        ds.filename = filename
        ds.streamID = streamID
        ds.vListID = vListID
        ds.tAxis = TAxisError()

        # register lists
        ds.grids = Vector{AbstractGrid}(undef, 0)
        ds.vars = Vector{Variable}(undef, 0)
        ds.zAxis = Vector{AbstractZAxis}(undef, 0)

        # register caches
        ds.varCacheName = Dict{AbstractString, Variable}()
        ds.varCacheID = Dict{Int64, Variable}()
        
        # register destructor
        finalizer(_finalize, ds)

        return ds
    end
end

############## private types ##############

"""
All elements of cdiDataset that could use in iterable context
"""
const cdiIterable = Vector{Variable}

############## public functions ##############

function Base.show(io::IO, ds::cdiDataset; indent="")
    printstyled(io, indent, "cdi Dataset ", bold=true, color=:red)
    print(io, indent, ds.filename, "\n\n")

    printstyled(io, "Grids: ", "\n", color=:light_red)
    for g in ds.grids
        show(io, g; indent="$(indent)  ")
        print(io, "\n")
    end

    printstyled(io, "T-Axis: ", "\n", color=:light_red)
    show(io, ds.tAxis; indent="$(indent)  ")
    print(io, "\n")

    printstyled(io, "Z-Axis: ", "\n", color=:light_red)
    for z in ds.zAxis
        show(io, z; indent="$(indent)  ")
        print(io, "\n")
    end
    
    printstyled(io, "Variables: ", "\n", color=:light_red)
    for v in ds.vars
        show(io, v; indent="$(indent)  ")
        print(io, "\n")
    end
end
export show

function Base.close(ds::cdiDataset)
    if ds.streamID >= 0
        cdic.streamClose(ds.streamID)
        ds.streamID = -1
    end
end
export close

"""
return the names of all keys in a Dataset that could select by iterable context
"""
function Base.keys(it::cdiIterable)
    return String[keys(i) for i in it]
end

"""
return the names of all Variable keys in a Dataset
"""
function Base.keys(ds::cdiDataset)
    k = Vector{String}(undef, 0)
    append!(k, keys(ds.vars))
    return k
end

Base.haskey(ds::cdiDataset, name) = name in keys(ds)
Base.in(name::AbstractString, it::cdiIterable) = name in keys(it)
Base.in(name::AbstractString, ds::cdiDataset) = name in keys(ds)
export keys, haskey, in

"""
Get the variable of the `ds` by name
"""
function Base.getindex(ds::cdiDataset, name::AbstractString)
    # if the key is in the cache return the variable direct
    if haskey(ds.varCacheName, name)
        return get(ds.varCacheName, name, nothing)
    end

    found = false
    for v in ds.vars
        push!(ds.varCacheName, keys(v) => v)
        if keys(v) == name
            found = true
        end
    end

    if found
        return get(ds.varCacheName, name, nothing)
    end

    throw(BoundsError(ds))
end

function Base.getindex(ds::cdiDataset, id::Integer)
     # if the key is in the cache return the variable direct
     if haskey(ds.varCacheID, id)
        return get(ds.varCacheID, id, nothing)
    end

    found = false
    for v in ds.vars
        push!(ds.varCacheID, v.varID => v)
        if v.varID == id
            found = true
        end
    end

    if found
        return get(ds.varCacheID, id, nothing)
    end

    throw(BoundsError(ds))
end
export getindex

function getTimestep(ds::cdiDataset)
    return getTimestep(ds.tAxis)
end
export getTimestep

"""
    tAxis(var)

Return the associated t-axis struct
"""
tAxis(ds::cdiDataset) = ds.tAxis
export tAxis

function cdiDataset(path::AbstractString, fileType, tAxis::AbstractTAxisTemplate,
    vars::Vector{VariableTemplate})::cdiDataset
    
    @debug "create vlist"
    #create libcdi vlist
    vlistID = vlistCreate()

    # create variables 
    for v in vars
        varID = vlistDefVar(vlistID, v.grid.gridID, v.zAxis.axisID, v.time)
        vlistDefVarName(vlistID, varID, v.name)
        v.code != -1     && vlistDefVarCode(vlistID, varID, v.code)
        v.stdName != ""  && vlistDefVarStdname(vlistID, varID, v.stdName)
        v.longName != "" && vlistDefVarLongname(vlistID, varID, v.longName)
        v.unit != -1     && vlistDefVarUnits(vlistID, varID, v.unit)
        # TODO set the other attributes
    end

    # create the t-axis
    t = TAxis(tAxis)

    # assign the Time axis to the variable list
    vlistDefTaxis(vlistID, t.axisID)

    streamID = streamOpenWrite(path, fileType);
    if streamID < 0
        @error cdiStringError(streamID)
        exit(1) # TODO Throw an expection
    end

    streamDefVlist(streamID, vlistID);

    # update vlistID
    vlistID = streamInqVlist(streamID)

    # create dataset
    ds = cdiDataset(path, streamID, vlistID)

    # load dataset metadata by libcdi to be shure that all works fine
    loadVariables!(ds)

    return ds
end