import Base.write, Base.show

"""
Global namespace for high level julia cdi interface
"""
module cdiDatasets

# include cdic only if it is not defined before
if !isdefined(@__MODULE__, :streamOpenRead)
    #include("./../cdic/cdic.jl")
    using ..cdic
end

# import types
include("grid.jl")
include("zAxis.jl")
include("tAxis.jl")
include("variable.jl")
include("dataset.jl")
include("dateTime.jl")

############## Connstructor ##############

function cdiDataset(filename::AbstractString)
    streamID = streamOpenRead(filename)
    if streamID < 0
        @error "Can't load file " * filename
        @error cdiStringError(streamID)
    end

    vlistID = streamInqVlist(streamID)
    @debug "get vlistID " * string(vlistID)
    ds = cdiDataset(filename, streamID, vlistID)

    loadVariables!(ds)

    return ds
end
export cdiDataset

"""
return true while there are timesteps
"""
function getNextTimestep(ds::cdiDataset)::Bool
    ds.tAxis.timestep.value += 1
    return (streamInqTimestep(ds.streamID, ds.tAxis.timestep.value -1) > 0)
end
export getNextTimestep

"""
set the date time for the next timestep. It will return the timestep id 
"""
function setNextTimestep!(ds::cdiDataset, dateTime::DateTime)
    # Set the verification date to 1985-01-01 + tsID
    taxisDefVdate(ds.tAxis.axisID, cdiEncodeDate(Date(dateTime)))

    # Set the verification time to 12:00:00
    taxisDefVtime(ds.tAxis.axisID, cdiEncodeTime(Time(dateTime)))

    # Define the time step
    ret = ds.tAxis.timestep.value
    streamDefTimestep(ds.streamID, ds.tAxis.timestep.value)
    ds.tAxis.timestep.value += 1
    
    return ret
end
export setNextTimestep!

############## public functions ##############


############## Helper functions ##############
function loadVariables!(ds::cdiDataset)
    @debug "load variables..."
    vars = vlistInqVars(ds.vListID)

    if isnothing(vars)
        @debug "can't found variables"
        return nothing
    end

    for v in vars
        varID = v[1]
        name = v[2]
        grid = loadGrid!(ds, varID)
        zAxis = loadZAxis!(ds, varID)
        tAxis = loadTAxis!(ds)
        streamID = ds.streamID

        missValue = vlistInqVarMissval(ds.vListID, varID)
        stdName = vlistInqVarStdname(ds.vListID, varID)
        longName = vlistInqVarLongname(ds.vListID, varID)
        unit = vlistInqVarUnits(ds.vListID, varID)
        code = vlistInqVarCode(ds.vListID, varID)

        append!(ds.vars, [Variable(varID, streamID, missValue, name, stdName, longName, unit, code, grid, zAxis, tAxis)])
    end

    return nothing
end

function loadGrid!(ds::cdiDataset, varID::Cint)::AbstractGrid
    gridID = vlistInqVarGrid(ds.vListID, varID)

    index = findfirst(g -> g.gridID == gridID, ds.grids)
    if index === nothing
        # create new grid
        l = loadGrid(gridID)
        append!(ds.grids, [l])
        return l
    else
        return ds.grids[index]
    end
end

function loadZAxis!(ds::cdiDataset, varID::Cint)::AbstractZAxis
    zID = vlistInqVarZaxis(ds.vListID, varID)

    index = findfirst(g -> g.axisID == zID, ds.zAxis)
    if index === nothing
        # create new zAxus
        z = loadZAxis(zID)
        
        append!(ds.zAxis, [z])
        return z
    else
        return ds.zAxis[index]
    end
end

function loadTAxis!(ds::cdiDataset)::AbstractTAxis

    if ds.tAxis === TAxisError()
        t = loadTAxis(ds.vListID)
        ds.tAxis = t
    end
    
    return ds.tAxis
end

end # module cdiDataset