import Base.size

if !isdefined(@__MODULE__, :ismatch)
    using Match
end

import ..TAXIS: TAXIS_t, ABSOLUTE, RELATIVE, FORECAST

############## Types ##############
abstract type AbstractTAxis end
abstract type AbstractTAxisTemplate end

# for internal stuff only
struct TAxisError <: AbstractTAxis end

mutable struct TAxisTimestep
    value::Integer
end

"""
Template Type to construct an cdiDataset
"""
struct TAxisTemplate <: AbstractTAxisTemplate
    type::TAXIS_t
end

"""
Template Type to duplicate an existing t-axis
"""
struct TAxisDuplicateTemplate <: AbstractTAxisTemplate
    blueprint::AbstractTAxis
end

# global types
struct TAxisAbsolut <: AbstractTAxis
    axisID::Cint
    timestep::TAxisTimestep
end
TAxisAbsolut() = createTAxis(ABSOLUTE)

struct TAxisRelativ <: AbstractTAxis
    axisID::Cint
    timestep::TAxisTimestep
end
TAxisRelativ() = createTAxis(RELATIVE)

struct TAxisForecast <: AbstractTAxis
    axisID::Cint
    timestep::TAxisTimestep
end
TAxisForecast() = createTAxis(FORECAST)

export TAxisAbsolut, TAxisRelativ, TAxisForecast, TAxisDuplicateTemplate

############## public functions ##############
function Base.show(io::IO, t::AbstractTAxis; indent="")
    printstyled(io, indent, "T-Axis: ", t.axisID, color=:light_green)
    printstyled(io, " (", tAxisType(t), ")", "\n", color=:blue)
end
export show

function getTimestep(axis::AbstractTAxis)
    return axis.timestep.value
end
export getTimestep

function resetTimestep(axis::AbstractTAxis)
    return axis.timestep.value = 0
end
export resetTimestep

"""
construct t-axis template from existing type `t <: AbstractTAxis`.
This template can pass as parameter to construct an cdiDataset
"""
duplicateTAxis(t::AbstractTAxis) = TAxisDuplicateTemplate(t)
export duplicateTAxis

############## Helper functions ##############

"""
load t-axis by vlistID
"""
function loadTAxis(vListID::Cint)::AbstractTAxis
    tID = vlistInqTaxis(vListID)
    type = tAxisType(taxisInqType(tID))
    return type(tID, TAxisTimestep())
end

"""
construct t-axis template. This template can pass as parameter to construct an cdiDataset
"""
function createTAxis(t::TAXIS_t)
    T = tAxisType(t)
    @assert T <: AbstractTAxis

    # TODO handle diffrent TAxis types

    return TAxisTemplate(t)
end

"""
construct an TAxis from an TAxisTemplate
"""
function TAxis(t::TAxisTemplate)
    T = tAxisType(t.type)
    @assert T <: AbstractTAxis

    # TODO handle diffrent TAxis types

    axisID = taxisCreate(t.type)

    return T(axisID, TAxisTimestep())
end

function TAxis(t::TAxisDuplicateTemplate)
    type = taxisInqType(t.blueprint.axisID)
    T = tAxisType(type)
    @assert T <: AbstractTAxis

    axisID = taxisDuplicate(t.blueprint.axisID)

    return T(axisID, TAxisTimestep())
end

"""
return the grid type enum value
"""
tAxisType(t::AbstractTAxis) = TAXIS_t(taxisInqType(t.axisID))

"""
return the julia grid Type like `LonLat <: AbstractGrid2D` from integer.
Integer must in the range of the enum cdic.GRID.GRID_t
"""
tAxisType(t::Integer) = tAxisType(TAXIS_t(t))

"""
convert the "c cdi" enum value to julia type like `LonLat <: AbstractGrid2D`
"""
tAxisType(t::TAXIS_t) = @match Int(t) begin
    1 => TAxisAbsolut
    2 => TAxisRelativ
    3 => TAxisForecast
    _ => TAxisError
end

"""
construct an timestep counter and init with 0
"""
TAxisTimestep() = TAxisTimestep(0)