
"""
    gridName(gridType::Number):String

Return the name of the grid type

# Parameter
- `gridType` gridType

# Return
- `String`: String of the Grid type

# Original C Prototype
- `void gridName(int gridtype, char *gridname)`
- `const char *gridNamePtr(int gridtype)`
"""
function gridName(gridType::Number):String
    name = ccall((:gridNamePtr, libcdi), Cstring, (Cint,), gridType)
    return unsafe_string(name)
end

# void    gridCompress(int gridID);

# void    gridDefMaskGME(int gridID, const int mask[]);
# int     gridInqMaskGME(int gridID, int mask[]);

# void    gridDefMask(int gridID, const int mask[]);
# int     gridInqMask(int gridID, int mask[]);

"""
    gridCreate(gridtype::GRID_t, size::Number):Cint

The function @func{gridCreate} creates a horizontal Grid.

# Arguments
- `gridtype`:   The type of the grid, one of the set of predefined CDI grid types.
- `size`:       Number of gridpoints.

# Return
- `Cint` returns an identifier to the Grid.

# Example
Here is an example using @func{gridCreate} to create a regular lon/lat Grid:

```julia
using cdi
#   ...
const lons = [0, 30, 60, 90, 120, 150, 180, 210, 240, 270, 300, 330]
const lats = [-75, -45, -15, 15, 45, 75]
#   ...
# Create a regular lon/lat grid
gridID = gridCreate(GRID.LONLAT, length(lons) * length(lats))
gridDefXsize(gridID, length(lons))
gridDefYsize(gridID, length(lats))
gridDefXvals(gridID, lons)
gridDefYvals(gridID, lats)
#   ...
```
"""
function gridCreate(gridtype::GRID_t, size::Number):Cint
    return ccall((:gridCreate, libcdi), Cint, (Cint, Csize_t), gridtype, size)
end

"""
    gridDestroy(gridID::Cint)

Destroy a horizontal Grid

# Parameter
- `gridID`:   Grid ID, from a previous call to @fref{gridCreate}.

# Original C Prototype
`void gridDestroy(int gridID)`
"""
function gridDestroy(gridID::Cint)
    ccall((:gridDestroy, libcdi), Cvoid, (Cint,), gridID)
end

"""
    gridDuplicate(gridID::Cint)::Cint

The function @func{gridDuplicate} duplicates a horizontal Grid.

# Parameter
- `gridID`:   Grid ID, from a previous call to @fref{gridCreate} or @fref{vlistInqVarGrid}.

# Return
`Cint` @func{gridDuplicate} returns an identifier to the duplicated Grid.

# Original C Prototype
`int gridDuplicate(int gridID)`
"""
function gridDuplicate(gridID::Cint)::Cint
    return ccall((:gridDuplicate, libcdi), Cint, (Cint,), gridID)
end

# //      gridDefProj: Define the projection ID of a Grid
# void    gridDefProj(int gridID, int projID);

# //      gridInqProj: Get the projection ID of a Grid
# int     gridInqProj(int gridID);

# //      gridInqProjType: Get the projection type
# int     gridInqProjType(int gridID);

# //      gridInqType: Get the type of a Grid
# int     gridInqType(int gridID);
function gridInqType(gridID::Cint)::Cint
    return ccall((:gridInqType, libcdi), Cint, (Cint,), gridID)
end

"""
    gridInqSize(gridID::Cint)

The function gridInqSize returns the number of values of a grid.

# Parameter
- `gridID`:  Grid ID, from a previous call to gridCreate or vlistInqVarGrid.

# Return
- gridInqSize returns the number of values of a grid.

# Original C Prototype
`size_t gridInqSize(int gridID);`
"""
function gridInqSize(gridID::Cint)::Csize_t
    return ccall((:gridInqSize, libcdi), Csize_t, (Cint,), gridID)
end
export gridInqSize

"""
    gridDefXsize(gridID::Cint, size::Number)

The function @func{gridDefXsize} defines the number of values of a X-axis.

# Parameter
-  `gridID`:  Grid ID, from a previous call to @fref{gridCreate}.
-  `xsize`:  Number of values of a X-axis.
"""
function gridDefXsize(gridID::Cint, size::Number)
    ccall((:gridDefXsize, libcdi), Cvoid, (Cint, Csize_t), gridID, size)
end

"""
    gridInqXsize(gridID::Cint)

The function gridInqXsize returns the number of values of a X-axis.

# Parameter
- `gridID`:  Grid ID, from a previous call to gridCreate or vlistInqVarGrid.

# Return
- gridInqXsize returns the number of values of a X-axis.

# Original C Prototype
`size_t gridInqXsize(int gridID);`
"""
function gridInqXsize(gridID::Cint):Csize_t
    return ccall((:gridInqXsize, libcdi), Csize_t, (Cint,), gridID)
end

"""
    gridDefYsize(gridID::Cint, size::Number)

The function @func{gridDefYsize} defines the number of values of a Y-axis.

# Parameter
- `gridID`:   Grid ID, from a previous call to @fref{gridCreate}.
- `ysize`:  Number of values of a Y-axis.
"""
function gridDefYsize(gridID::Cint, size::Number)
    ccall((:gridDefYsize, libcdi), Cvoid, (Cint, Csize_t), gridID, size)
end

"""
    gridInqYsize(gridID::Cint)

The function gridInqYsize returns the number of values of a Y-axis.

# Parameter
- `gridID`:  Grid ID, from a previous call to gridCreate or vlistInqVarGrid.

# Return
- gridInqYsize returns the number of values of a Y-axis.

# Original C Prototype
`size_t gridInqYsize(int gridID);`
"""
function gridInqYsize(gridID::Cint):Csize_t
    ccall((:gridInqYsize, libcdi), Csize_t, (Cint,), gridID)
end

# //      gridDefNP: Define the number of parallels between a pole and the equator
# void    gridDefNP(int gridID, int np);

# //      gridInqNP: Get the number of parallels between a pole and the equator
# int     gridInqNP(int gridID);

"""
    gridDefXvals(gridID::Cint, xvals::Vector{Cdouble})

The function @func{gridDefXvals} defines all values of the X-axis.

# Parameter
- `gridID`:     Grid ID, from a previous call to @fref{gridCreate}.
- `xvals`:      X-values of the grid.

# Original C Prototype
`void gridDefXvals(int gridID, const double *xvals)`
"""
function gridDefXvals(gridID::Cint, xvals::Vector{Cdouble})
    ccall((:gridDefXvals, libcdi), Cvoid, (Cint, Ptr{Cdouble}), gridID, xvals)
end

function gridDefXvals(gridID::Cint, xvals::Vector{T}) where {T <: Number}
    gridDefXvals(gridID, convert(Vector{Cdouble}, xvals))
end

"""
    gridInqXvals(gridID::Cint)

The function gridInqXvals returns all values of the X-axis.

# Parameter
- `gridID`:  Grid ID, from a previous call to gridCreate or vlistInqVarGrid.

# Return
- A vector with all grid x vals

# Original C Prototype
`size_t gridInqXvals(int gridID, double *xvals);`
"""
function gridInqXvals(gridID::Cint)::Vector{Cdouble}
    size = gridInqXsize(gridID)
    vals = Vector{Cdouble}(undef, size)
    ccall((:gridInqXvals, libcdi), Csize_t, (Cint, Ptr{Cdouble}), gridID, vals)
    return vals
end


# size_t  gridInqXvalsPart(int gridID, int start, size_t size, double xvals[]);

# //      gridInqXIsc: Find out whether X-coordinate is of type CHAR
# int     gridInqXIsc(int gridID);

# //      gridInqXCvals: Get strings from X-axis in case grid is of type GRID_CHARXY
# size_t  gridInqXCvals(int gridID, char *xcvals[]);

"""
    gridDefYvals(gridID::Cint, yvals::Vector{T}) where {T <: Number}

The function @func{gridDefYvals} defines all values of the Y-axis.

# Parameter
- `gridID`: Grid ID, from a previous call to @fref{gridCreate}.
- `yvals`:  Y-values of the grid.

# Original C Prototype
`void gridDefYvals(int gridID, const double *yvals)`
"""
function gridDefYvals(gridID::Cint, yvals::Vector{Cdouble})
    ccall((:gridDefYvals, libcdi), Cvoid, (Cint, Ptr{Cdouble}), gridID, yvals)
end

function gridDefYvals(gridID::Cint, yvals::Vector{T}) where {T <: Number}
    gridDefYvals(gridID, convert(Vector{Cdouble}, yvals))
end

"""
    gridInqYvals(gridID::Cint)

The function gridInqYvals returns all values of the X-axis.

# Parameter
- `gridID`:  Grid ID, from a previous call to gridCreate or vlistInqVarGrid.

# Return
- A vector with all grid x vals

# Original C Prototype
`size_t gridInqYvals(int gridID, double *xvals);`
"""
function gridInqYvals(gridID::Cint)::Vector{Cdouble}
    size = gridInqYsize(gridID)
    vals = Vector{Cdouble}(undef, size)
    ccall((:gridInqYvals, libcdi), Csize_t, (Cint, Ptr{Cdouble}), gridID, vals)
    return vals
end

# //      gridInqYIsc: Find out whether Y-coordinate is of type CHAR
# int     gridInqYIsc(int gridID);

# //      gridInqYCvals: Get strings from Y-axis in case grid is of type GRID_CHARXY
# size_t  gridInqYCvals(int gridID, char *ycvals[]);

function gridDefTrunc(gridID::Cint, trunc::Integer)
    ccall((:gridDefTrunc, libcdi), Cvoid, (Cint, Cint), gridID, trunc)
end
export gridDefTrunc

function gridDefComplexPacking(gridID::Cint, lcomplex::Integer)
    ccall((:gridDefComplexPacking, libcdi), Cvoid, (Cint, Cint), gridID, lcomplex)
end
export gridDefComplexPacking

# Export Symbols
foreach(names(@__MODULE__, all=true)) do s
    if startswith(string(s), "grid")
        @eval export $s
    end
end
