# // ZAXIS routines

"""
    zaxisCreate(zaxistype::ZAXIS_t, size::Number):Cint

The function @func{zaxisCreate} creates a vertical Z-axis.

# Parameter
- `zaxistype`:  The type of the Z-axis, one of the set of predefined CDI Z-axis types.
- `size`:       Number of levels.

# Return
- `Cint` returns an identifier to the Z-axis.

# Example
Here is an example using @func{zaxisCreate} to create a pressure level Z-axis:

```julia
using cdi
using cdi.ZAXIS
#   ...
const levs = [101300, 92500, 85000, 50000, 20000]
#   ...
zaxisID = zaxisCreate(ZAXIS.PRESSURE, length(levs))
zaxisDefLevels(zaxisID, levs)
#   ...
```

# Original C Prototype
`int zaxisCreate(int zaxistype, int size)`
"""
function zaxisCreate(zaxistype::ZAXIS_t, size::Number):Cint
    return ccall((:zaxisCreate, libcdi), Cint, (Cint, Cint), zaxistype, size)
end

"""
    zaxisDestroy(zaxisID::Cint)

Destroy a vertical Z-axis

# Parameter
- `zaxisID`:  Z-axis ID, from a previous call to @fref{zaxisCreate}.

# Original C Prototype
`void zaxisDestroy(int zaxisID)`
"""
function zaxisDestroy(zaxisID::Cint)
    ccall((:zaxisDestroy, libcdi), Cvoid, (Cint,), zaxisID)
end

# //      zaxisInqType: Get the type of a Z-axis
# int     zaxisInqType(int zaxisID);
function zaxisInqType(zaxisID::Cint)::Cint
    return ccall((:zaxisInqType, libcdi), Cint, (Cint,), zaxisID)
end

"""
zaxisInqSize(zaxisID::Cint)::Cint

Return the number of levels by this axis

# Parameter
- `zaxisID`:  Z-axis ID, from a previous call to @fref{zaxisCreate}.

# Original C Prototype
`int zaxisInqSize(int zaxisID);`
"""
function zaxisInqSize(zaxisID::Cint)::Cint
    return ccall((:zaxisInqSize, libcdi), Cint, (Cint,), zaxisID)
end

"""
zaxisDuplicate(gridID::Cint)::Cint

The function @func{zaxisDuplicate} duplicates a pressure axis.

# Parameter
- `zaxisID`: Pressure axis ID, from a previous call to @func{zaxisCreate} that shuld duplicate

# Return
`Cint` @func{zaxisDuplicate} returns an identifier to the duplicated pressure axis.

# Original C Prototype
`int zaxisDuplicate(int zaxisID)`
"""
function zaxisDuplicate(zaxisID::Cint)::Cint
    return ccall((:zaxisDuplicate, libcdi), Cint, (Cint,), zaxisID)
end

# //      zaxisDefLevels: Define the levels of a Z-axis
# void    zaxisDefLevels(int zaxisID, const double levels[]);

"""
    zaxisDefLevels(zaxisID::Cint, levels::Vector{Cdouble})

The function @func{zaxisDefLevels} defines the levels of a Z-axis.

# Parameter
- `zaxisID`:    Z-axis ID, from a previous call to @fref{zaxisCreate}.
- `levels`:     All levels of the Z-axis.

# Original C Prototype
`void zaxisDefLevels(int zaxisID, const double *levels)`
"""
function zaxisDefLevels(zaxisID::Cint, levels::Vector{Cdouble})
    ccall((:zaxisDefLevels, libcdi), Cvoid, (Cint, Ptr{Cdouble}), zaxisID, levels)
end

function zaxisDefLevels(zaxisID::Cint, levels::Vector{T}) where {T <: Number}
    zaxisDefLevels(zaxisID, convert(Vector{Cdouble}, levels))
end

# //      zaxisDefCvals: Define area types of a Z-axis
# void    zaxisDefCvals(int zaxisID, const char *cvals[], int clength);

# //      zaxisInqLevels: Get all levels of a Z-axis
# int     zaxisInqLevels(int zaxisID, double levels[]);
"""
zaxisInqLevels(zaxisID::Cint)

The function zaxisInqLevels returns all values of the X-axis.

# Parameter
- `zaxisID`:   Z-axis ID, from a previous call to zaxisCreate or vlistInqVarZaxis.

# Return
- A vector with all levels

# Original C Prototype
`size_t zaxisInqLevels(int zaxisID, double *xvals);`
"""
function zaxisInqLevels(zaxisID::Cint)::Vector{Cdouble}
    size = zaxisInqSize(zaxisID)
    vals = Vector{Cdouble}(undef, size)
    ccall((:zaxisInqLevels, libcdi), Csize_t, (Cint, Ptr{Cdouble}), zaxisID, vals)
    return vals
end

# //      zaxisInqCLen: Get maximal string length of character Z-axis
# int     zaxisInqCLen(int zaxisID);

# //      zaxisInqCVals: Get all string values of a character Z-axis
# int     zaxisInqCVals(int zaxisID, char ***clevels);

# //      zaxisDefLevel: Define one level of a Z-axis
# void    zaxisDefLevel(int zaxisID, int levelID, double levels);

# //      zaxisInqLevel: Get one level of a Z-axis
# double  zaxisInqLevel(int zaxisID, int levelID);

# //      zaxisDefNlevRef: Define the number of half levels of a generalized Z-axis
# void    zaxisDefNlevRef(int gridID, int nhlev);

# //      zaxisInqNlevRef: Get the number of half levels of a generalized Z-axis
# int     zaxisInqNlevRef(int gridID);

# //      zaxisDefNumber: Define the reference number for a generalized Z-axis
# void    zaxisDefNumber(int gridID, int number);

# //      zaxisInqNumber: Get the reference number to a generalized Z-axis
# int     zaxisInqNumber(int gridID);

# //      zaxisDefUUID: Define the UUID of a generalized Z-axis
# void    zaxisDefUUID(int zaxisID, const unsigned char uuid[CDI_UUID_SIZE]);

# //      zaxisInqUUID: Get the UUID of a generalized Z-axis
# void    zaxisInqUUID(int zaxisID, unsigned char uuid[CDI_UUID_SIZE]);

# //      zaxisDefName: Define the name of a Z-axis
# void    zaxisDefName(int zaxisID, const char *name_optional);

# //      zaxisInqName: Get the name of a Z-axis
# void    zaxisInqName(int zaxisID, char *name);
function zaxisInqName(zaxisID::Cint)::String
    len = 512 # MAXNAMELEN
    name = Vector{UInt8}(undef, len) 
    
    ccall((:zaxisInqName, libcdi), Cvoid,
                (Cint, Ptr{UInt8}),
                zaxisID, name)

    name[end] = 0 # ensure null-termination
    return unsafe_string(pointer(name))
end

# //      zaxisDefLongname: Define the longname of a Z-axis
# void    zaxisDefLongname(int zaxisID, const char *longname_optional);

# //      zaxisInqLongname: Get the longname of a Z-axis
# void    zaxisInqLongname(int zaxisID, char *longname);
function zaxisInqLongname(zaxisID::Cint)::String
    len = 512 # MAXNAMELEN
    name = Vector{UInt8}(undef, len) 
    
    ccall((:zaxisInqLongname, libcdi), Cvoid,
                (Cint, Ptr{UInt8}),
                zaxisID, name)

    name[end] = 0 # ensure null-termination
    return unsafe_string(pointer(name))
end

# //      zaxisDefUnits: Define the units of a Z-axis
# void    zaxisDefUnits(int zaxisID, const char *units_optional);

"""
    zaxisInqUnits(zaxisID::Cint)::String

The function zaxisInqUnits returns the units of a Z-axis.

# Parameter
- `zaxisID`:  Z-axis ID, from a previous call to zaxisCreate or vlistInqVarZaxis.

# Return
Unit as String

# Original C Prototype
`void zaxisInqUnits(int zaxisID, char *units);`
"""
function zaxisInqUnits(zaxisID::Cint)::String
    len = 512 # MAXNAMELEN
    name = Vector{UInt8}(undef, len) 
    
    ccall((:zaxisInqUnits, libcdi), Cvoid,
                (Cint, Ptr{UInt8}),
                zaxisID, name)

    name[end] = 0 # ensure null-termination
    return unsafe_string(pointer(name))
end

# //      zaxisInqStdname: Get the standard name of a Z-axis
# void    zaxisInqStdname(int zaxisID, char *stdname);

# void    zaxisDefDatatype(int zaxisID, int prec);
# int     zaxisInqDatatype(int zaxisID);

# void    zaxisDefPositive(int zaxisID, int positive);
# int     zaxisInqPositive(int zaxisID);

# void    zaxisDefScalar(int zaxisID);
# int     zaxisInqScalar(int zaxisID);

# void    zaxisDefLtype(int zaxisID, int ltype);
# int     zaxisInqLtype(int zaxisID);

# void    zaxisDefLtype2(int zaxisID, int ltype2);
# int     zaxisInqLtype2(int zaxisID);

# void    zaxisDefVct(int zaxisID, int size, const double vct[]);
function zaxisDefVct(zaxisID, vct::Vector{T}) where {T <: Number}
    zaxisDefVct(zaxisID, convert(Vector{Cdouble}, vct))
end

function zaxisDefVct(zaxisID, vct::Vector{Cdouble})
    ccall((:zaxisDefVct, libcdi), Cvoid, (Cint, Cint, Ptr{Cdouble}), zaxisID, length(vct), vct)
end

# int     zaxisInqVctSize(int zaxisID);
function zaxisInqVctSize(zaxisID::Cint)
    return ccall((:zaxisInqVctSize, libcdi), Cint, (Cint,), zaxisID)
end

# void    zaxisInqVct(int zaxisID, double vct[]);
function zaxisInqVct(zaxisID)
    vct = Vector{Cdouble}(undef, zaxisInqVctSize(zaxisID))
    ccall((:zaxisInqVct, libcdi), Cvoid, (Cint, Ptr{Cdouble}), zaxisID, vct)
    return vct
end

# const double *zaxisInqVctPtr(int zaxisID);
# void    zaxisDefLbounds(int zaxisID, const double lbounds[]);
function zaxisDefLbounds(zaxisID, lowerBound::Vector{T}) where {T <: Number}
    zaxisDefLbounds(zaxisID, convert(Vector{Cdouble}, lowerBound))
end

function zaxisDefLbounds(zaxisID, lowerBound::Vector{Cdouble})
    ccall((:zaxisDefLbounds, libcdi), Cvoid, (Cint, Ptr{Cdouble}), zaxisID, lowerBound)
end

# int     zaxisInqLbounds(int zaxisID, double lbounds_optional[]);
function zaxisInqLbounds(zaxisID)
    bound = Vector{Cdouble}(undef, zaxisInqSize(zaxisID))
    length = ccall((:zaxisInqLbounds, libcdi), Cint, (Cint, Ptr{Cdouble}), zaxisID, bound)
    resize!(bound, length)
    return bound
end

# double  zaxisInqLbound(int zaxisID, int index);

# void    zaxisDefUbounds(int zaxisID, const double ubounds[]);
function zaxisDefUbounds(zaxisID, upperBound::Vector{T}) where {T <: Number}
    zaxisDefUbounds(zaxisID, convert(Vector{Cdouble}, upperBound))
end

function zaxisDefUbounds(zaxisID, upperBound::Vector{Cdouble})
    ccall((:zaxisDefUbounds, libcdi), Cvoid, (Cint, Ptr{Cdouble}), zaxisID, upperBound)
end

# int     zaxisInqUbounds(int zaxisID, double ubounds_optional[]);
function zaxisInqUbounds(zaxisID)
    bound = Vector{Cdouble}(undef, zaxisInqSize(zaxisID))
    length = ccall((:zaxisInqUbounds, libcdi), Cint, (Cint, Ptr{Cdouble}), zaxisID, bound)
    resize!(bound, length)
    return bound
end

# double  zaxisInqUbound(int zaxisID, int index);


# void    zaxisDefWeights(int zaxisID, const double weights[]);
# int     zaxisInqWeights(int zaxisID, double weights_optional[]);
# void    zaxisChangeType(int zaxisID, int zaxistype);

# Export Symbols
foreach(names(@__MODULE__, all=true)) do s
    if startswith(string(s), "zaxis")
        @eval export $s
    end
end