"""
    vlistCreate():Cint

Create a variable list

# Example
Here is an example using @func{vlistCreate} to create a variable list
and add a variable with @func{vlistDefVar}.

```julia
using cdi
#   ...
vlistID = vlistCreate();
varID = vlistDefVar(vlistID, gridID, zaxisID, TIME_VARYING);
#   ...
streamDefVlist(streamID, vlistID);
#   ...
vlistDestroy(vlistID);
#   ...
```

# Original C Prototype
`int vlistCreate(void)`
"""
function vlistCreate():Cint
    ccall((:vlistCreate, libcdi), Cint, ())
end

"""
    vlistDestroy(vlistID::Cint)

Destroy a variable list

# Parameter
- `vlistID`:  Variable list ID, from a previous call to @fref{vlistCreate}.

# Original C Prototype
`void vlistDestroy(int vlistID)`
"""
function vlistDestroy(vlistID::Cint)
    ccall((:vlistDestroy, libcdi), Cvoid, (Cint,), vlistID)
end

# //      vlistDuplicate: Duplicate a variable list
# int     vlistDuplicate(int vlistID);

# //      vlistCopy: Copy a variable list
# void    vlistCopy(int vlistID2, int vlistID1);

# //      vlistCopyFlag: Copy some entries of a variable list
# void    vlistCopyFlag(int vlistID2, int vlistID1);

# void    vlistClearFlag(int vlistID);

# //      vlistCat: Concatenate two variable lists
# void    vlistCat(int vlistID2, int vlistID1);

# //      vlistMerge: Merge two variable lists
# void    vlistMerge(int vlistID2, int vlistID1);

# void    vlistPrint(int vlistID);

# //      vlistNumber: Number type in a variable list
# int     vlistNumber(int vlistID);

"""
    vlistNvars(vlistID::Cint):Cint

Get the Number of variables in a variable list

# Parameter
- `vlistID`: Variable list ID, from a previous call to @fref{vlistCreate}.

# Return
- Number of variables in a variable list

# Original C Prototype
`int vlistNvars(int vlistID);`
"""
function vlistNvars(vlistID::Cint):Cint
    return ccall((:vlistNvars, libcdi), Cint, (Cint,), vlistID)
end

# //      vlistNgrids: Number of grids in a variable list
# int     vlistNgrids(int vlistID);

# //      vlistNzaxis: Number of zaxis in a variable list
# int     vlistNzaxis(int vlistID);

# //      vlistNsubtypes: Number of subtypes in a variable list
# int     vlistNsubtypes(int vlistID);

# void    vlistDefNtsteps(int vlistID, int nts);

"""
vlistNtsteps(vlistID::Cint):Cint

Get the Number of timesteps in a variable list if known 

# Parameter
- `vlistID`: Variable list ID, from a previous call to @fref{vlistCreate}.

# Return
- Number of timestaps in a variable list or -1 if unknown

# Original C Prototype
`int vlistNtsteps(int vlistID);`
"""
function vlistNtsteps(vlistID::Cint):Cint
    return ccall((:vlistNtsteps, libcdi), Cint, (Cint,), vlistID)
end

"""
    vlistInqNTimesteps(vlistID::Cint, streamID::Cint)

Get the number of timestamps in stream. If the stream has a netCDF file in the 
background this will be a O(1) function. If the file is not netCDF it will be O(n). 
All records will be read once.

# Parameter
- `vlistID`: Variable list ID, from a previous call to @fref{vlistCreate}.
- `streamID`: Stream ID 

# Return
- number of timestamps
"""
function vlistInqNTimesteps(vlistID::Cint, streamID::Cint):Cint
    steps = vlistNtsteps(vlistID)

    if steps >= 0
        return steps
    end

    steps = 0
    while streamInqTimestep(streamID, steps) > 0
        steps += 1
    end
    
    return convert(Cint, steps)
end

# size_t  vlistGridsizeMax(int vlistID);
# int     vlistGrid(int vlistID, int index);
# int     vlistGridIndex(int vlistID, int gridID);
# void    vlistChangeGridIndex(int vlistID, int index, int gridID);
# void    vlistChangeGrid(int vlistID, int gridID1, int gridID2);
# int     vlistZaxis(int vlistID, int index);
# int     vlistZaxisIndex(int vlistID, int zaxisID);
# void    vlistChangeZaxisIndex(int vlistID, int index, int zaxisID);
# void    vlistChangeZaxis(int vlistID, int zaxisID1, int zaxisID2);
# int     vlistNrecs(int vlistID);
# int     vlistSubtype(int vlistID, int index);
# int     vlistSubtypeIndex(int vlistID, int subtypeID);

"""
    vlistDefTaxis(vlistID::Cint, taxisID::Cint)

The function @func{vlistDefTaxis} defines the time axis of a variable list.

# Parameter
- `vlistID`:  Variable list ID, from a previous call to @fref{vlistCreate}.
- `taxisID`:  Time axis ID, from a previous call to @fref{taxisCreate}.

# Original C Prototype
`void vlistDefTaxis(int vlistID, int taxisID)`
"""
function vlistDefTaxis(vlistID::Cint, taxisID::Cint)
    ccall((:vlistDefTaxis, libcdi), Cvoid, (Cint, Cint), vlistID, taxisID)
end

"""
    vlistInqTaxis(vlistID::Cint)::Cint

# Parameter
- `vlistID`:  Variable list ID, from a previous call to @fref{vlistCreate} or @fref{streamInqVlist}.

@Description
The function @func{vlistInqTaxis} returns the time axis of a variable list.

# Return
`Cint` @func{vlistInqTaxis} returns an identifier to the time axis.

# Original C Prototype
`int vlistInqTaxis(int vlistID)`
"""
function vlistInqTaxis(vlistID::Cint)::Cint
    return ccall((:vlistInqTaxis, libcdi), Cint, (Cint,), vlistID)
end

# void    vlistDefTable(int vlistID, int tableID);
# int     vlistInqTable(int vlistID);
# void    vlistDefInstitut(int vlistID, int instID);
# int     vlistInqInstitut(int vlistID);
# void    vlistDefModel(int vlistID, int modelID);
# int     vlistInqModel(int vlistID);

# // VLIST VAR routines

# //      vlistDefVarTiles: Create a new tile-based variable
# int     vlistDefVarTiles(int vlistID, int gridID, int zaxisID, int timetype, int tilesetID);

"""
    listDefVar(vlistID::Cint, gridID::Cint, zaxisID::Cint, timetype::TIME_t)

The function @func{vlistDefVar} adds a new variable to vlistID.

# Parameter
- `vlistID`:   Variable list ID, from a previous call to @fref{vlistCreate}.
- `gridID`:    Grid ID, from a previous call to @fref{gridCreate}.
- `zaxisID`:   Z-axis ID, from a previous call to @fref{zaxisCreate}.
- `timetype`:  One of the set of predefined CDI timestep types.

# Return 
- `Cint` returns an identifier to the new variable.

@Example
Here is an example using @func{vlistCreate} to create a variable list
and add a variable with @func{vlistDefVar}.

```julia
using cdi
using cdi.TIME
#   ...
vlistID = vlistCreate();
varID = vlistDefVar(vlistID, gridID, zaxisID, VARYING);
#   ...
streamDefVlist(streamID, vlistID);
#   ...
vlistDestroy(vlistID);
#   ...
```

# Original C Prototype
`int vlistDefVar(int vlistID, int gridID, int zaxisID, int timetype)`
"""
function vlistDefVar(vlistID::Cint, gridID::Cint, zaxisID::Cint, timetype::TIME_t):Cint
    return ccall((:vlistDefVar, libcdi), Cint, (Cint, Cint, Cint, Cint), vlistID, gridID, zaxisID, timetype)
end

# void    vlistChangeVarGrid(int vlistID, int varID, int gridID);
# void    vlistChangeVarZaxis(int vlistID, int varID, int zaxisID);

# void    vlistInqVar(int vlistID, int varID, int *gridID, int *zaxisID, int *timetype);
"""
    vlistInqVarGrid(vlistID::Cint, varID::Cint)

The function vlistInqVarGrid returns the grid ID of a Variable.

# Parameter
- `vlistID`:  Variable list ID, from a previous call to vlistCreate or streamInqVlist.
- `varID`: Variable identifier

# Return
- return the grid ID of the variable

# Original C Prototype
`int vlistInqVarGrid(int vlistID, int varID);`
"""
function vlistInqVarGrid(vlistID::Cint, varID::Cint):Cint
    return ccall((:vlistInqVarGrid, libcdi), Cint, (Cint, Cint), vlistID, varID)
end

"""
    vlistInqVarZaxis(vlistID::Cint, varID::Cint)

The function vlistInqVarZaxis returns the zaxis ID of a variable.

# Parameter
- `vlistID`:   Variable list ID, from a previous call to @fref{vlistCreate}.
- `varID`:    Variable identifier.

# Return
- vlistInqVarZaxis returns the zaxis ID of the variable.

# Original C Prototype
`int vlistInqVarZaxis(int vlistID, int varID)`
"""
function vlistInqVarZaxis(vlistID::Cint, varID::Cint):Cint
    return ccall((:vlistInqVarZaxis, libcdi), Cint, (Cint, Cint), vlistID, varID)
end

# //      used in MPIOM
# int     vlistInqVarID(int vlistID, int code);

# void    vlistDefVarTimetype(int vlistID, int varID, int timetype);
# int     vlistInqVarTimetype(int vlistID, int varID);

# void    vlistDefVarTsteptype(int vlistID, int varID, int tsteptype);

# //      vlistInqVarTsteptype: Get the timestep type of a Variable
# int     vlistInqVarTsteptype(int vlistID, int varID);

# void    vlistDefVarCompType(int vlistID, int varID, int comptype);
# int     vlistInqVarCompType(int vlistID, int varID);
# void    vlistDefVarCompLevel(int vlistID, int varID, int complevel);
# int     vlistInqVarCompLevel(int vlistID, int varID);

# //      vlistDefVarParam: Define the parameter number of a Variable
# void    vlistDefVarParam(int vlistID, int varID, int param);

# //      vlistInqVarParam: Get the parameter number of a Variable
# int     vlistInqVarParam(int vlistID, int varID);

"""
vlistDefVarCode(vlistID::Cint, varID::Cint, code::Integer)

The function @func{vlistDefVarCode} defines the code number of a variable.

# Parameter
- `vlistID`:  Variable list ID, from a previous call to @fref{vlistCreate}.
- `varID`:    Variable identifier.
- `code`:     code of the variable.

# Original C Prototype
`void vlistDefVarCode(int vlistID, int varID, int code)`
"""
function vlistDefVarCode(vlistID::Cint, varID::Cint, code::Integer)
    ccall((:vlistDefVarCode, libcdi), Cvoid, (Cint, Cint, Cint), vlistID, varID, code)
end

# //      vlistInqVarCode: Get the code number of a Variable
# int     vlistInqVarCode(int vlistID, int varID);
"""
    vlistInqVarCode(vlistID::Cint, varID::Cint)

The function vlistInqVarCode returns the code number of a variable.

# Parameter
- `vlistID`:   Variable list ID, from a previous call to @fref{vlistCreate}.
- `varID`:    Variable identifier.

# Return
- vlistInqVarCode returns the code number of the variable.

# Original C Prototype
`int vlistInqVarCode(int vlistID, int varID)`
"""
function vlistInqVarCode(vlistID::Cint, varID::Cint)
    return ccall((:vlistInqVarCode, libcdi), Cint, (Cint, Cint), vlistID, varID)
end

# //      vlistDefVarDatatype: Define the data type of a Variable
# void    vlistDefVarDatatype(int vlistID, int varID, int datatype);

# //      vlistInqVarDatatype: Get the data type of a Variable
# int     vlistInqVarDatatype(int vlistID, int varID);

# void    vlistDefVarChunkType(int vlistID, int varID, int chunktype);
# int     vlistInqVarChunkType(int vlistID, int varID);

# void    vlistDefVarXYZ(int vlistID, int varID, int xyz);
# int     vlistInqVarXYZ(int vlistID, int varID);

# int     vlistInqVarNumber(int vlistID, int varID);

# void    vlistDefVarInstitut(int vlistID, int varID, int instID);
# int     vlistInqVarInstitut(int vlistID, int varID);
# void    vlistDefVarModel(int vlistID, int varID, int modelID);
# int     vlistInqVarModel(int vlistID, int varID);
# void    vlistDefVarTable(int vlistID, int varID, int tableID);
# int     vlistInqVarTable(int vlistID, int varID);

# //      vlistDefVarName: Define the name of a Variable
# void    vlistDefVarName(int vlistID, int varID, const char *name);
"""
    vlistDefVarName(vlistID::Cint, varID::Cint, name::AbstractString)

The function @func{vlistDefVarName} defines the name of a variable.

# Parameter
- `vlistID`:  Variable list ID, from a previous call to @fref{vlistCreate}.
- `varID`:    Variable identifier.
- `name`:     Name of the variable.

# Original C Prototype
`void vlistDefVarName(int vlistID, int varID, const char *name)`
"""
function vlistDefVarName(vlistID::Cint, varID::Cint, name::AbstractString)
    ccall((:vlistDefVarName, libcdi), Cvoid, (Cint, Cint, Cstring), vlistID, varID, name)
end

"""
    vlistInqVarName(vlistID::Integer, varID::Integer)::String

# Parameter
- `vlistID`:  Variable list ID, from a previous call to @fref{vlistCreate}.
- `varID`:    Variable identifier.

# Return
- String with the name of the variable or empty string if there was an error. 

# Original C Prototype
`void vlistInqVarName(int vlistID, int varID, char *name)`
"""
function vlistInqVarName(vlistID::Integer, varID::Integer)::String
    len = 512 # MAXNAMELEN
    name = Vector{UInt8}(undef, len) 
    
    ccall((:vlistInqVarName, libcdi), Cvoid, (Cint, Cint, Ptr{UInt8}), vlistID, varID, name)

    name[end] = 0 # ensure null-termination
    return unsafe_string(pointer(name))
end

"""
vlistInqVarLongname(vlistID::Integer, varID::Integer)::String

# Parameter
- `vlistID`:  Variable list ID, from a previous call to @fref{vlistCreate}.
- `varID`:    Variable identifier.

# Return
- String with the name of the variable or empty string if there was an error. 

# Original C Prototype
`void vlistInqVarLongname(int vlistID, int varID, char *name)`
"""
function vlistInqVarLongname(vlistID::Integer, varID::Integer)::String
    len = 512 # MAXNAMELEN
    name = Vector{UInt8}(undef, len) 
    
    ccall((:vlistInqVarLongname, libcdi), Cvoid, (Cint, Cint, Ptr{UInt8}), vlistID, varID, name)

    name[end] = 0 # ensure null-termination
    return unsafe_string(pointer(name))
end

"""
vlistInqVarStdname(vlistID::Integer, varID::Integer)::String

# Parameter
- `vlistID`:  Variable list ID, from a previous call to @fref{vlistCreate}.
- `varID`:    Variable identifier.

# Return
- String with the name of the variable or empty string if there was an error. 

# Original C Prototype
`void vlistInqVarStdname(int vlistID, int varID, char *name)`
"""
function vlistInqVarStdname(vlistID::Integer, varID::Integer)::String
    len = 512 # MAXNAMELEN
    name = Vector{UInt8}(undef, len) 
    
    ccall((:vlistInqVarStdname, libcdi), Cvoid, (Cint, Cint, Ptr{UInt8}), vlistID, varID, name)

    name[end] = 0 # ensure null-termination
    return unsafe_string(pointer(name))
end

"""
vlistInqVarUnits(vlistID::Integer, varID::Integer)::String

# Parameter
- `vlistID`:  Variable list ID, from a previous call to @fref{vlistCreate}.
- `varID`:    Variable identifier.

# Return
- String with the unit of the variable or empty string if there was an error. 

# Original C Prototype
`void vlistInqVarUnits(int vlistID, int varID, char *name)`
"""
function vlistInqVarUnits(vlistID::Integer, varID::Integer)::String
    len = 512 # MAXNAMELEN
    name = Vector{UInt8}(undef, len) 
    
    ccall((:vlistInqVarUnits, libcdi), Cvoid, (Cint, Cint, Ptr{UInt8}), vlistID, varID, name)

    name[end] = 0 # ensure null-termination
    return unsafe_string(pointer(name))
end

"""
    vlistInqVars(vlistID::Cint)::Vector{Tuple{Integer, String}}

Return a list of all used variable names and ids in this vlist.

# Parameter
- `vlistID`: Variable list ID, from a previous call to @fref{vlistCreate}.

# Return
- `array` Array that contains all used varIDs and varNames as tuple. 
"""
function vlistInqVars(vlistID::Cint)::Vector{Tuple{Cint,String}}
    vars = Vector{Tuple{Cint,String}}(undef, 0)
    numOfVars = vlistNvars(vlistID)

    for i in 0:(numOfVars - 1)
        append!(vars, [(convert(Cint, i), vlistInqVarName(vlistID, i))])
    end

    return vars
end

"""
    vlistInqVarId(vlistID::Cint, name::AbstractString)

Return the varID in a vlist by name

# Parameter
- `vlistID`: Variable list ID, from a previous call to @fref{vlistCreate}.
- `name`: Name of the Variable

# Return
- VarID or nothing if the variable is not defined
"""
function vlistInqVarId(vlistID::Cint, name::AbstractString)
    vars = vlistInqVars(vlistID)
    index = findfirst(x -> x[2] == name, vars)

    if isnothing(index)
        return nothing
    end

    return vars[index][1]
end

# //      vlistCopyVarName: Safe and convenient version of vlistInqVarName
# char   *vlistCopyVarName(int vlistId, int varId);

"""
    vlistDefVarStdname(vlistID::Cint, varID::Cint, stdname::AbstractString)

The function @func{vlistDefVarStdname} defines the stdname of a variable.

# Parameter
- `vlistID`:  Variable list ID, from a previous call to @fref{vlistCreate}.
- `varID`:    Variable identifier.
- `stdname`:     Stdname of the variable.

# Original C Prototype
`void vlistDefVarStdname(int vlistID, int varID, const char *name)`
"""
function vlistDefVarStdname(vlistID::Cint, varID::Cint, stdname::AbstractString)
    ccall((:vlistDefVarStdname, libcdi), Cvoid, (Cint, Cint, Cstring), vlistID, varID, stdname)
end

"""
vlistDefVarLongname(vlistID::Cint, varID::Cint, longname::AbstractString)

The function @func{vlistDefVarLongname} defines the long name of a variable.

# Parameter
- `vlistID`:  Variable list ID, from a previous call to @fref{vlistCreate}.
- `varID`:    Variable identifier.
- `longname`:     long name of the variable.

# Original C Prototype
`void vlistDefVarLongname(int vlistID, int varID, const char *name)`
"""
function vlistDefVarLongname(vlistID::Cint, varID::Cint, longname::AbstractString)
    ccall((:vlistDefVarLongname, libcdi), Cvoid, (Cint, Cint, Cstring), vlistID, varID, longname)
end



# //      vlistDefVarUnits: Define the units of a Variable
# void    vlistDefVarUnits(int vlistID, int varID, const char *units);
"""
vlistDefVarUnits(vlistID::Cint, varID::Cint, unit::AbstractString)

The function @func{vlistDefVarUnits} defines the long name of a variable.

# Parameter
- `vlistID`:  Variable list ID, from a previous call to @fref{vlistCreate}.
- `varID`:    Variable identifier.
- `unit`:     name of the unit.

# Original C Prototype
`void vlistDefVarUnits(int vlistID, int varID, const char *unit)`
"""
function vlistDefVarUnits(vlistID::Cint, varID::Cint, unit::AbstractString)
    ccall((:vlistDefVarUnits, libcdi), Cvoid, (Cint, Cint, Cstring), vlistID, varID, unit)
end

# //      vlistDefVarMissval: Define the missing value of a Variable
# void    vlistDefVarMissval(int vlistID, int varID, double missval);

# //      vlistInqVarMissval: Get the missing value of a Variable
# double  vlistInqVarMissval(int vlistID, int varID);
function vlistInqVarMissval(vlistID::Cint, varID::Cint)
    return ccall((:vlistInqVarMissval, libcdi), Cdouble, (Cint, Cint), vlistID, varID)
end

# //      vlistDefVarExtra: Define extra information of a Variable
# void    vlistDefVarExtra(int vlistID, int varID, const char *extra);

# //      vlistInqVarExtra: Get extra information of a Variable
# void    vlistInqVarExtra(int vlistID, int varID, char *extra);

# void    vlistDefVarScalefactor(int vlistID, int varID, double scalefactor);
# double  vlistInqVarScalefactor(int vlistID, int varID);
# void    vlistDefVarAddoffset(int vlistID, int varID, double addoffset);
# double  vlistInqVarAddoffset(int vlistID, int varID);

# void    vlistDefVarTimave(int vlistID, int varID, int timave);
# int     vlistInqVarTimave(int vlistID, int varID);

# size_t  vlistInqVarSize(int vlistID, int varID);

# void    vlistDefIndex(int vlistID, int varID, int levID, int index);
# int     vlistInqIndex(int vlistID, int varID, int levID);
# void    vlistDefFlag(int vlistID, int varID, int levID, int flag);
# int     vlistInqFlag(int vlistID, int varID, int levID);
# int     vlistFindVar(int vlistID, int fvarID);
# int     vlistFindLevel(int vlistID, int fvarID, int flevelID);
# int     vlistMergedVar(int vlistID, int varID);
# int     vlistMergedLevel(int vlistID, int varID, int levelID);

"""
    vlistDefVarAttr(vlistID::Cint, varID::Cint, name::String, longname::String, unit::String, code::Number, stdname::String = name)

Set the attributes of a value

# Parameter
- `vlistID`: Variable list ID, from a previous call to @fref{vlistCreate}.
- `varID`: Variable ID
- `name`: Name of the variable
- `longname`: Name of the variable
- `unit`: Unit of the variable
- `code`: Code of the variable
- `stdname`: stdname of the variable if set. Otherwise it is equal to name
"""
function vlistDefVarAttr(vlistID::Cint, varID::Cint, name::String, longname::String, unit::String, code::Number, stdname::String = name)
    vlistDefVarName(vlistID, varID, name)
    vlistDefVarStdname(vlistID, varID, stdname)
    vlistDefVarLongname(vlistID, varID, longname)
    vlistDefVarCode(vlistID, varID, code)
    vlistDefVarUnits(vlistID, varID, unit)
end

# Export Symbols
foreach(names(@__MODULE__, all=true)) do s
    if startswith(string(s), "vlist")
        @eval export $s
    end
end