"""
Global namespace for low level julia cdi interface
"""
module cdic

import Libdl

# create libcdi symbole
@static if Sys.iswindows()
    @error "Windows is currently not supported"
    const libcdi = ""
elseif Sys.isapple()
    const libcdi = Libdl.find_library(["libcdi", "cdi"])
elseif Sys.islinux()
    const libcdi = Libdl.find_library(["libcdi", "cdi"])
else
    @warn "Your platform couldn't detected. Use the fallback"
    const libcdi = Libdl.find_library(["libcdi", "cdi"])
end

@assert libcdi != "" "Can't load libcdi at DL_LOAD_PATH"
#@info "Load libcdi successful"

# include macros
#include("cdi_macro.jl")
#@info names(@__MODULE__, all=true, imported=true)
import ..TIME: TIME_t
import ..GRID: GRID_t
import ..ZAXIS: ZAXIS_t
import ..TAXIS: TAXIS_t
import ..FileTypes: FileTypes_t

#include functions
include("grid.jl")
include("zaxis.jl")
include("vlsit.jl")
include("taxis.jl")
include("stream.jl")


function cdiStringError(cdiErrno::Integer)
    str = ccall((:cdiStringError, libcdi), Cstring, (Cint,), cdiErrno)
    return unsafe_string(str)
end
export cdiStringError

end #module cdic
