# // TAXIS routines

"""
    taxisCreate(taxistype::TAXIS_t)

The function @func{taxisCreate} creates a Time axis.

# Parameter
- `taxistype`:  The type of the Time axis, one of the set of predefined CDI time axis types.

# Return
`Cint` returns an identifier to the Time axis.

# Example
Here is an example using @func{taxisCreate} to create a relative T-axis with a standard calendar.

```
using cdi
using cdi.TAXIS
using cdi.CALENDAR
#   ...
taxisID = taxisCreate(TAXIS.RELATIVE);
taxisDefCalendar(taxisID, CALENDAR.STANDARD);
taxisDefRdate(taxisID, 19850101);
taxisDefRtime(taxisID, 120000);
#   ...
```

# Original C Prototype
`int taxisCreate(int taxistype)`
"""
function taxisCreate(taxistype::TAXIS_t)
    return ccall((:taxisCreate, libcdi), Cint, (Cint,), taxistype)
end

"""
    taxisDestroy(taxisID::Cint)

Destroy a Time axis

# Parameter
- `taxisID`  Time axis ID, from a previous call to @func{taxisCreate}

# Original C Prototype
`void taxisDestroy(int taxisID)`
"""
function taxisDestroy(taxisID::Cint)
    ccall((:taxisDestroy, libcdi), Cvoid, (Cint,), taxisID)
end

"""
    taxisDuplicate(gridID::Cint)::Cint

The function @func{taxisDuplicate} duplicates a time axis.

# Parameter
- `taxisID`  Time axis ID, from a previous call to @func{taxisCreate} that shuld duplicate

# Return
`Cint` @func{taxisDuplicate} returns an identifier to the duplicated time axis.

# Original C Prototype
`int taxisDuplicate(int taxisID)`
"""
function taxisDuplicate(taxisID::Cint)::Cint
    return ccall((:taxisDuplicate, libcdi), Cint, (Cint,), taxisID)
end

# void    taxisCopyTimestep(int taxisIDdes, int taxisIDsrc);

# void    taxisDefType(int taxisID, int taxistype);
# int     taxisInqType(int taxisID);
function taxisInqType(taxisID::Cint)::Cint
    return ccall((:taxisInqType, libcdi), Cint, (Cint,), taxisID)
end

"""
    taxisDefVdate(taxisID::Cint, date::Int64)

The function @func{taxisDefVdate} defines the verification date of a Time axis.

# Parameter
- `taxisID`:  Time axis ID, from a previous call to @fref{taxisCreate}
- `vdate`:    Verification date (YYYYMMDD)

# Original C Prototype
`void taxisDefVdate(int taxisID, int64_t vdate)`
"""
function taxisDefVdate(taxisID::Cint, date::Int64)
    ccall((:taxisDefVdate, libcdi), Cvoid, (Cint, Int64), taxisID, date)
end

"""
    taxisDefVtime(taxisID::Cint, time::Number)

The function @func{taxisDefVtime} defines the verification time of a Time axis.

# Parameter
- `taxisID`:  Time axis ID, from a previous call to @fref{taxisCreate}
- `vtime`:    Verification time (hhmmss)

#Original C Prototype
`void taxisDefVtime(int taxisID, int vtime)`
"""
function taxisDefVtime(taxisID::Cint, time::Number)
    ccall((:taxisDefVtime, libcdi), Cvoid, (Cint, Cint), taxisID, time)
end

"""
    taxisInqVdate(taxisID::Cint)::Int64

The function @func{taxisInqVdate} returns the verification date of a Time axis.

# Parameter
- `taxisID`:  Time axis ID, from a previous call to @fref{taxisCreate} or @fref{vlistInqTaxis}

# Return
`Int64` @func{taxisInqVdate} returns the verification date.

# Original C Prototype
`int64_t taxisInqVdate(int taxisID)`
"""
function taxisInqVdate(taxisID::Cint)::Int64
    return ccall((:taxisInqVdate, libcdi), Int64, (Cint,), taxisID)
end

"""
    taxisInqVtime(taxisID::Cint)::Int64

The function @func{taxisInqVtime} returns the verification time of a Time axis.

# Parameter
- `taxisID`:  Time axis ID, from a previous call to @fref{taxisCreate} or @fref{vlistInqTaxis}

# Return
`Int64` @func{taxisInqVtime} returns the verification time.#

# Original C Prototype
`int taxisInqVtime(int taxisID)`
"""
function taxisInqVtime(taxisID::Cint)::Int64
    return ccall((:taxisInqVtime, libcdi), Int64, (Cint,), taxisID)
end

# //      taxisDefRdate: Define the reference date
# void    taxisDefRdate(int taxisID, int64_t date);

# //      taxisDefRtime: Define the reference time
# void    taxisDefRtime(int taxisID, int time);

# //      taxisInqRdate: Get the reference date
# int64_t taxisInqRdate(int taxisID);

# //      taxisInqRtime: Get the reference time
# int     taxisInqRtime(int taxisID);

# //      taxisDefFdate: Define the forecast reference date
# void    taxisDefFdate(int taxisID, int64_t date);

# //      taxisDefFtime: Define the forecast reference time
# void    taxisDefFtime(int taxisID, int time);

# //      taxisInqFdate: Get the forecast reference date
# int64_t taxisInqFdate(int taxisID);

# //      taxisInqFtime: Get the forecast reference time
# int     taxisInqFtime(int taxisID);

# int     taxisHasBounds(int taxisID);
# void    taxisWithBounds(int taxisID);

# void    taxisDeleteBounds(int taxisID);

# void    taxisDefVdateBounds(int taxisID, int64_t vdate_lb, int64_t vdate_ub);

# void    taxisDefVtimeBounds(int taxisID, int vtime_lb, int vtime_ub);

# void    taxisInqVdateBounds(int taxisID, int64_t *vdate_lb, int64_t *vdate_ub);

# void    taxisInqVtimeBounds(int taxisID, int *vtime_lb, int *vtime_ub);

# //      taxisDefCalendar: Define the calendar
# void    taxisDefCalendar(int taxisID, int calendar);

# //      taxisInqCalendar: Get the calendar
# int     taxisInqCalendar(int taxisID);

# void    taxisDefTunit(int taxisID, int tunit);
# int     taxisInqTunit(int taxisID);

# void    taxisDefForecastTunit(int taxisID, int tunit);
# int     taxisInqForecastTunit(int taxisID);

# void    taxisDefForecastPeriod(int taxisID, double fc_period);
# double  taxisInqForecastPeriod(int taxisID);

# void    taxisDefNumavg(int taxisID, int numavg);
# int     taxisInqNumavg(int taxisID);

# const char *tunitNamePtr(int tunitID);

# Export Symbols
foreach(names(@__MODULE__, all=true)) do s
    if startswith(string(s), "taxis")
        @eval export $s
    end
end

using Dates

function cdiDecodeDate(date::Integer)::Date
    year = Ref{Cint}(0)
    month = Ref{Cint}(0)
    day = Ref{Cint}(0)
    ccall((:cdiDecodeDate, libcdi), Cvoid, (Int64, Ref{Cint}, Ref{Cint}, Ref{Cint}), date, year, month, day)
    return Date(year[], month[], day[])
end

function cdiEncodeDate(date::Date)::Int64
    return ccall((:cdiEncodeDate, libcdi), Int64, (Cint, Cint, Cint),  Dates.year(date), Dates.month(date), Dates.day(date))
end

function cdiDecodeTime(time::Integer)::Time
    hour = Ref{Cint}(0)
    minute = Ref{Cint}(0)
    second = Ref{Cint}(0)
    ccall((:cdiDecodeTime, libcdi), Cvoid, (Cint, Ref{Cint}, Ref{Cint}, Ref{Cint}), time, hour, minute, second)
    return Time(hour[], minute[], second[])
end

function cdiEncodeTime(time::Time)::Cint
    return ccall((:cdiEncodeTime, libcdi), Cint, (Cint, Cint, Cint),  Dates.hour(time), Dates.minute(time), Dates.second(time))
end

export cdiDecodeDate, cdiEncodeDate, cdiDecodeTime, cdiEncodeTime