# // STREAM control routines

# int     cdiGetFiletype(const char *path, int *byteorder);

"""
    streamOpenRead(path::AbstractString):Cint

The function @func{streamOpenRead} opens an existing dataset for reading.

# Parameter
- `path`:  The name of the dataset to be read.

# Return
`Cint` Upon successful completion @func{streamOpenWrite} returns an identifier to the open stream. Otherwise, a negative number with the error status is returned.

## Errors
- `CDI_ESYSTEM`:     Operating system error.
- `CDI_EINVAL`:      Invalid argument.
- `CDI_EUFILETYPE`:  Unsupported file type.
- `CDI_ELIBNAVAIL`:  Library support not compiled in.

# Example
Here is an example using @func{streamOpenRead} to open an existing NetCDF
file named @func{foo.nc} for reading:

```julia
using cdi
#   ...
streamID = streamOpenRead("example.nc")
if streamID < 0
    @error cdiStringError(streamID)
    exit(streamID)
end
#   ...
```

# Original C Prototype
`int streamOpenRead(const char *path)`
"""
function streamOpenRead(path::AbstractString):Cint
    return ccall((:streamOpenRead, libcdi), Cint, (Cstring,), path)
end

"""
    streamOpenWrite(path::AbstractString, filetype::FileTypes_t):Cint

The function @func{streamOpenWrite} creates a new datset.

# Parameter
- `path`:      The name of the new dataset.
- `filetype`:  The type of the file format, one of the set of predefined CDI file format types.

# Return
`Cint` Upon successful completion @func{streamOpenWrite} returns an identifier to the open stream. Otherwise, a negative number with the error status is returned.

## Errors
- `CDI_ESYSTEM`:     Operating system error.
- `CDI_EINVAL`:      Invalid argument.
- `CDI_EUFILETYPE`:  Unsupported file type.
- `CDI_ELIBNAVAIL`:  Library support not compiled in.

# Example
Here is an example using @func{streamOpenWrite} to create a new NetCDF file named @func{foo.nc} for writing:

```julia
using cdi
using cdi.FileTypes
#   ...
streamID = streamOpenWrite("example.nc", FileTypes.FILETYPE_NC);
if streamID < 0
    @error cdiStringError(streamID)
    exit(streamID)
end
```

# Original C Prototype
`int streamOpenWrite(const char *path, int filetype)`
"""
function streamOpenWrite(path::AbstractString, filetype::FileTypes_t):Cint
    return ccall((:streamOpenWrite, libcdi), Cint, (Cstring, Cint), path, filetype)
end

# int     streamOpenAppend(const char *path);

"""
    streamClose(streamID::Cint)

The function @func{streamClose} closes an open dataset.

# Parameter
- `streamID`:  Stream ID, from a previous call to @fref{streamOpenRead} or @fref{streamOpenWrite}.

# Original C Prototype
`void streamClose(int streamID)`
"""
function streamClose(streamID::Cint)
    ccall((:streamClose, libcdi), Cvoid, (Cint,), streamID)
end

# //      streamSync: Synchronize an Open Dataset to Disk
# void    streamSync(int streamID);

# void    streamDefNumWorker(int streamID, int numWorker);

"""
    streamDefVlist(streamID::Cint, vlistID::Cint)

The function @func{streamDefVlist} defines the variable list of a stream.

To safeguard against errors by modifying the wrong vlist object,
this function makes the passed vlist object immutable.
All further vlist changes have to use the vlist object returned by `streamInqVlist()`.

# Parameter
- `streamID`: Stream ID, from a previous call to @fref{streamOpenWrite}.
- `vlistID`:  Variable list ID, from a previous call to @fref{vlistCreate}.

# Original C Prototype
`void streamDefVlist(int streamID, int vlistID)`
"""
function streamDefVlist(streamID::Cint, vlistID::Cint)
    ccall((:streamDefVlist, libcdi), Cvoid, (Cint, Cint), streamID, vlistID)
end

"""
    streamInqVlist(streamID::Cint)::Cint

The function @func{streamInqVlist} returns the variable list of a stream

# Parameter
- `streamID`:  Stream ID, from a previous call to @fref{streamOpenRead} or @fref{streamOpenWrite}.

# Return
`Cint`@func{streamInqVlist} returns an identifier to the variable list.

# Original C Prototype
`int streamInqVlist(int streamID)`
"""
function streamInqVlist(streamID::Cint)::Cint
    return ccall((:streamInqVlist, libcdi), Cint, (Cint,), streamID)
end

# //      streamInqFiletype: Get the filetype
# int     streamInqFiletype(int streamID);

# //      streamDefByteorder: Define the byteorder
# void    streamDefByteorder(int streamID, int byteorder);

# //      streamInqByteorder: Get the byteorder
# int     streamInqByteorder(int streamID);

# //      streamDefCompType: Define compression type
# void    streamDefCompType(int streamID, int comptype);

# //      streamInqCompType: Get compression type
# int     streamInqCompType(int streamID);

# //      streamDefCompLevel: Define compression level
# void    streamDefCompLevel(int streamID, int complevel);

# //      streamInqCompLevel: Get compression level
# int     streamInqCompLevel(int streamID);

# //      streamDefTimestep: Define time step
# int     streamDefTimestep(int streamID, int tsID);
"""
    streamDefTimestep(streamID::Cint, tsID::Number)::Cint

The function @func{streamDefTimestep} defines a timestep of a stream by the identifier tsID.
The identifier tsID is the timestep index starting at 0 for the first timestep.
Before calling this function the functions @func{taxisDefVdate} and @func{taxisDefVtime} should be used
to define the timestamp for this timestep. All calls to write the data refer to this timestep.

# Parameter
- `streamID`:  Stream ID, from a previous call to @fref{streamOpenWrite}.
- `tsID`:      Timestep identifier.

# Return
`Cint` @func{streamDefTimestep} returns the number of expected records of the timestep.

# Original C Prototyp
`int streamDefTimestep(int streamID, int tsID)`
"""
function streamDefTimestep(streamID::Cint, tsID::Number)::Cint
    return ccall((:streamDefTimestep, libcdi), Cint, (Cint, Cint), streamID, tsID)
end

"""
    streamInqTimestep(streamID::Cint, tsID::Integer)::Cint

The function @func{streamInqTimestep} sets the next timestep to the identifier tsID.
The identifier tsID is the timestep index starting at 0 for the first timestep.
After a call to this function the functions @func{taxisInqVdate} and @func{taxisInqVtime} can be used
to read the timestamp for this timestep. All calls to read the data refer to this timestep.

# Parameter
- `streamID`  Stream ID, from a previous call to @fref{streamOpenRead} or @fref{streamOpenWrite}.
- `tsID`:      Timestep identifier.

# Return
`Cint` @func{streamInqTimestep} returns the number of records of the timestep or 0, if the end of the file is reached.

# Original C Prototype
`int streamInqTimestep(int streamID, int tsID)`
"""
function streamInqTimestep(streamID::Cint, tsID::Integer)::Cint
    return ccall((:streamInqTimestep, libcdi), Cint, (Cint,Cint), streamID, tsID)
end

# //      PIO: query currently set timestep id
# int     streamInqCurTimestepID(int streamID);

# //      streamWriteVar: Write a variable
# void    streamWriteVar(int streamID, int varID, const double data[], size_t nmiss);
# void    streamWriteVarF(int streamID, int varID, const float data[], size_t nmiss);
"""
    streamWriteVar(streamID::Cint, varID::Cint, data::Vector{Cdouble}, nmiss::Number)

The function streamWriteVar writes the values of one time step of a variable to an open dataset.
The values are converted to the external data type of the variable, if necessary.

# Parameter
- `streamID`:  Stream ID, from a previous call to @fref{streamOpenWrite}.
- `varID`:     Variable identifier.
- `data`:      Pointer to a block of double precision floating point data values to be written.
- `nmiss`:     Number of missing values.

# Original C Prototype
`void streamWriteVar(int streamID, int varID, const double *data, size_t nmiss)`

`void streamWriteVar(int streamID, int varID, const float *data, size_t nmiss)`

"""
function streamWriteVar(streamID::Cint, varID::Cint, data::Vector{Cdouble}, nmiss::Number)
    ccall((:streamWriteVar, libcdi), Cvoid, (Cint, Cint, Ptr{Cdouble}, Csize_t), streamID, varID, data, nmiss)
end

function streamWriteVar(streamID::Cint, varID::Cint, data::Vector{Cfloat}, nmiss::Number)
    ccall((:streamWriteVar, libcdi), Cvoid, (Cint, Cint, Ptr{Cfloat}, Csize_t), streamID, varID, data, nmiss)
end

function streamWriteVar(streamID::Cint, varID::Cint, data::Vector{T}, nmiss::Number) where {T <: Number}
    streamWriteVar(streamID, varID, convert(Vector{Cdouble}, data), nmiss)
end

"""
    streamReadVar(streamID::Cint, varID::Cint, data::Vector{Cdouble})::Cint

The function streamReadVar reads all the values of one time step of a variable
from an open dataset.

# Parameter
- `streamID`:     Stream ID, from a previous call to @fref{streamOpenRead}.
- `varID`:        Variable identifier.
- `data`:    Pointer to the location into which the data values are read. The caller
must allocate space for the returned values.

# Return
- `Cint`:        Number of missing values.

# Original C Prototype
`streamReadVar(int streamID, int varID, double data[], size_t *nmiss)`

`streamReadVarF(int streamID, int varID, float data[], size_t *nmiss)`
"""
function streamReadVar(streamID::Cint, varID::Cint, data::Vector{Cdouble})::Cint
    miss = Ref{Csize_t}(0)
    ccall((:streamReadVar, libcdi), Int64, (Cint, Cint, Ptr{Cdouble}, Ref{Csize_t}) ,streamID, varID, data, miss)
    return miss[]
end
function streamReadVar(streamID::Cint, varID::Cint, data::Vector{Cfloat})::Cint
    miss = Ref{Csize_t}(0)
    ccall((:streamReadVarF, libcdi), Int64, (Cint, Cint, Ptr{Cfloat}, Ref{Csize_t}) ,streamID, varID, data, miss)
    return miss[]
end
# void    streamReadVarPart(int streamID, int varID, int varType, int start, size_t size, void *data, size_t *nmiss, int memtype);

# //      streamWriteVarSlice: Write a horizontal slice of a variable
# void    streamWriteVarSlice(int streamID, int varID, int levelID, const double data[], size_t nmiss);
# void    streamWriteVarSliceF(int streamID, int varID, int levelID, const float data[], size_t nmiss);
# void    streamReadVarSlicePart(int streamID, int varID, int levelID, int varType, int start, size_t size, void *data, size_t *nmiss, int memtype);

# //      streamReadVarSlice: Read a horizontal slice of a variable
# void    streamReadVarSlice(int streamID, int varID, int levelID, double data[], size_t *nmiss);
# void    streamReadVarSliceF(int streamID, int varID, int levelID, float data[], size_t *nmiss);

# void    streamWriteVarChunk(int streamID, int varID, const int rect[3][2], const double data[], size_t nmiss);


# Export Symbols
foreach(names(@__MODULE__, all=true)) do s
    if startswith(string(s), "stream")
        @eval export $s
    end
end