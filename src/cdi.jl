module cdi

# export the importend modules from "cdic/cdi_macro.jl" as pre decleration
export TIME, GRID, ZAXIS, ZAXIS, FileTypes
include("cdic/cdi_macro.jl")

export cdic
include("cdic/cdic.jl")
using .cdic

export cdiDatasets
include("cdiDataset/cdiDatasets.jl")
using .cdiDatasets

end #module cdi
